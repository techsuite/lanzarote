package app.company.components;

import static org.assertj.core.api.Assertions.assertThat;

import app.GenericTest;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class NavBarComponentTest extends GenericTest {

  private NavBarComponent navBarComponent;
  private static final String HOME_PAGE_URL =
    "https://techsuite.gitlab.io/company-fe";

  @Before
  public void navBarSetup() {
    driver.get(HOME_PAGE_URL);
    navBarComponent = new NavBarComponent(driver);
  }

  @Test
  public void checkLiterals() {
    List<String> literals = navBarComponent.getLiterals();

    assertThat(literals.get(0)).isEqualTo("Services");
    assertThat(literals.get(1)).isEqualTo("Technologies");
    assertThat(literals.get(2)).isEqualTo("Clients");
    assertThat(literals.get(3)).isEqualTo("Team");
    assertThat(literals.get(4)).isEqualTo("Work with us");
    
  }

  @Test
  public void checkNavigation() {
    String currentUrl = HOME_PAGE_URL;

    navBarComponent.clickOn("Services");
    assertThat(driver.getCurrentUrl()).isEqualTo(currentUrl + "/services");

    navBarComponent.clickOn("Technologies");
    assertThat(driver.getCurrentUrl()).isEqualTo(currentUrl + "/technologies");

    navBarComponent.clickOn("Clients");
    assertThat(driver.getCurrentUrl()).isEqualTo(currentUrl + "/clients");

    navBarComponent.clickOn("Team");
    assertThat(driver.getCurrentUrl()).isEqualTo(currentUrl + "/team");

    navBarComponent.clickOn("Work with us");
    assertThat(driver.getCurrentUrl()).isEqualTo(currentUrl + "/work-with-us");
  }
}
