package app.company.components;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import app.GenericTest;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class TechnologiesComponentTest extends GenericTest {

	private TechnologiesComponent TechnologiesComponent;
	private static final String HOME_PAGE_URL = "https://techsuite.gitlab.io/company-fe/technologies";

	@Before
	public void TechnologiesSetup() {
		driver.get(HOME_PAGE_URL);
		TechnologiesComponent = new TechnologiesComponent(driver);
	}

	@Test
	public void checkLiterals() {
		String titulo = TechnologiesComponent.getTitle();
		String subtitulo = TechnologiesComponent.getSubtitle();
		String linea = TechnologiesComponent.getLine();
		List<String> iconos = TechnologiesComponent.getIcons();

		// Título
		assertNotNull(titulo);
		assertThat(titulo).isEqualTo("Our Technologies");

		// Subtítulo
		assertNotNull(subtitulo);
		assertThat(subtitulo)
				.isEqualTo("We offer cutting-edge technology and a business approach with agile methodology.");

		// Línea
		assertNotNull(linea);
		assertThat(linea).isEqualTo("Some examples of our technologies are:");

		// Iconos
		for (int i = 0; i < 6; i++) {
			assertNotNull(iconos.get(i));
		}
		assertThat(iconos.get(0)).isEqualTo("Angular");
		assertThat(iconos.get(1)).isEqualTo("Gitlab");
		assertThat(iconos.get(2)).isEqualTo("Mysql");
		assertThat(iconos.get(3)).isEqualTo("Spring Boot");
		assertThat(iconos.get(4)).isEqualTo("Selenium");
		assertThat(iconos.get(5)).isEqualTo("Docker");

	}

}
