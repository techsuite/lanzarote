package app.company.components;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import app.GenericTest;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class ServicesComponentTest extends GenericTest {

	private ServicesComponent ServicesComponent;
	private static final String HOME_PAGE_URL = "https://techsuite.gitlab.io/company-fe/services";
	
	@Before
	public void ServicesSetup() {
		driver.get(HOME_PAGE_URL);
		ServicesComponent = new ServicesComponent(driver);
	}

	@Test
	public void checkLiterals() {
		String titulo = ServicesComponent.getTitle();
		String subtitulo = ServicesComponent.getSubtitle();
		List<String> iconos = ServicesComponent.getIcons();

		// Título
		assertNotNull(titulo);
		assertThat(titulo).isEqualTo("Our Services");

		// Subtítulo
		assertNotNull(subtitulo);
		assertThat(subtitulo).isEqualTo("We offer innovative software with outstanding user interface/experience.");

		// Iconos
		for (int i = 0; i < 3; i++) {
			assertNotNull(iconos.get(i));
		}
		assertThat(iconos.get(0)).isEqualTo("BackEnd Dev");
		assertThat(iconos.get(1)).isEqualTo("FrontEnd Dev");
		assertThat(iconos.get(2)).isEqualTo("Mobile Dev");
	}

	@Test
	public void checkGoogleLogo() {
		assertNotNull(ServicesComponent.getGoogleLogo("assets/google.png"));
	}

	@Test
	public void checkDescriptionServices() {
		assertThat(ServicesComponent.getDescription())
				.isEqualTo("Don't waste your time, hire professionals, hire Techsuite.");
	}

	@Test
	public void checkGoogleLinks() {
		String mainTab = driver.getWindowHandle();

		ServicesComponent.clickOnGoogleLink("Hire A Developer");
		String handlewindow = (String) driver.getWindowHandles().toArray()[1];
		driver.switchTo().window(handlewindow);

		assertThat(driver.getCurrentUrl()).startsWith("https://www.google.com/search?q=hire%20a%20developer");

		driver.close();
		driver.switchTo().window(mainTab);

		ServicesComponent.clickOnGoogleLink("Software Company");
		handlewindow = (String) driver.getWindowHandles().toArray()[1];
		driver.switchTo().window(handlewindow);

		assertThat(driver.getCurrentUrl()).startsWith("https://www.google.com/search?q=software%20company");

		driver.close();
		driver.switchTo().window(mainTab);

		ServicesComponent.clickOnGoogleLink("Build Web Application");
		handlewindow = (String) driver.getWindowHandles().toArray()[1];
		driver.switchTo().window(handlewindow);

		assertThat(driver.getCurrentUrl()).startsWith("https://www.google.com/search?q=build%20web%20application");
	}

}