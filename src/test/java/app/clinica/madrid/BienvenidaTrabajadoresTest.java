package app.clinica.madrid;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import app.GenericTest;

public class BienvenidaTrabajadoresTest extends GenericTest {

	private BienvenidaTrabajadores BienvenidaTrabajadores;
	private static final String HOME_PAGE_URL = "https://techsuite.gitlab.io/madrid";

	@Before
	public void BienvenidaMedicosSetup() {
		driver.get(HOME_PAGE_URL);
		BienvenidaTrabajadores = new BienvenidaTrabajadores(driver);
	}

	@Test
	public void checkLiterals() {

		String titulo = BienvenidaTrabajadores.getTitle();
		String texto = BienvenidaTrabajadores.getText();

		// Titulo
		assertNotNull(titulo);
		assertThat(titulo).isEqualTo("Bienvenido a la Clínica April");
		// Subtítulo
		assertNotNull(texto);
		assertThat(texto).isEqualTo(
				"El ambiente de trabajo de la Clínica April destaca por sus dinámicas de grupo que hacen que cada profesional se sienta parte de una gran comunidad sanitaria. Introduzca sus credenciales, pulsando en “Iniciar sesión”, para consultar los datos de sus pacientes.");

	}

	@Test
	public void checkTechsuiteLogo() {
		assertNotNull(BienvenidaTrabajadores
				.getTechsuiteLogo("https://drive.google.com/uc?id=11S5p_sDmewpkVvXgnvdmj8SmkhX1RIJf"));
	}

	@Test
	public void checkIniciarSesionButton() {
		String currentUrl = HOME_PAGE_URL;

		BienvenidaTrabajadores.clickOn("Iniciar Sesión");
		assertThat(driver.getCurrentUrl()).isEqualTo(currentUrl + "/login");

	}

	@Test
	public void checkNavBar() {

		List<String> literals = BienvenidaTrabajadores.getLiterals();

		assertThat(literals.get(0)).isEqualTo("Inicio");
		assertThat(literals.get(1)).isEqualTo("Login");
	}

	@Test
	public void checkNavigation() {

		String currentUrl = HOME_PAGE_URL;

		BienvenidaTrabajadores.clickOnNavBar("Inicio ");
		assertThat(driver.getCurrentUrl()).isEqualTo(currentUrl + "/");

		BienvenidaTrabajadores.clickOnNavBar("Login ");
		assertThat(driver.getCurrentUrl()).isEqualTo(currentUrl + "/login");
	}

}
