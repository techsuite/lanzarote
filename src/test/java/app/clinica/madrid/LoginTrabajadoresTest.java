package app.clinica.madrid;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import app.GenericTest;

public class LoginTrabajadoresTest extends GenericTest {

	private LoginTrabajadores LoginTrabajadores;
	private static final String HOME_PAGE_URL = "https://techsuite.gitlab.io/madrid/login";
	private String user = "00000000v";
	private String pssw = "asdf";

	public void checkErrorMsg() {
		String msg = LoginTrabajadores.getErrorMsg();
		assertNotNull(msg);
		assertThat(msg).isEqualTo("Usuario y/o contraseña incorrecta");
	}

	@Before
	public void LoginMedicosSetup() {
		driver.get(HOME_PAGE_URL);
		LoginTrabajadores = new LoginTrabajadores(driver);
	}

	@Test
	public void checkLogin() throws InterruptedException {
		String raizUrl = "https://techsuite.gitlab.io/madrid";
		LoginTrabajadores.login(user, pssw);

		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(raizUrl + "/home/bienvenida");
	}

	@Test
	// password vacio user correcto
	public void checkWrongLogin1() throws InterruptedException {

		LoginTrabajadores.login(user, "");
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(HOME_PAGE_URL);
		checkErrorMsg();
	}

	@Test
	// user vacio password correcto
	public void checkWrongLogin2() throws InterruptedException {

		LoginTrabajadores.login("", pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(HOME_PAGE_URL);
		checkErrorMsg();
	}

	@Test
	// user espacios password correcto
	public void checkWrongLogin3() throws InterruptedException {

		LoginTrabajadores.login("      ", pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(HOME_PAGE_URL);
		checkErrorMsg();
	}

	@Test
	// user correcto password espacios
	public void checkWrongLogin4() throws InterruptedException {

		LoginTrabajadores.login(user, "      ");
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(HOME_PAGE_URL);
	}

	@Test
	// user String aleatorio password correcto
	public void checkWrongLogin5() throws InterruptedException {

		LoginTrabajadores.login("miguel", pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(HOME_PAGE_URL);
		checkErrorMsg();
	}

	@Test
	// user correcto password String aleatorio
	public void checkWrongLogin6() throws InterruptedException {

		LoginTrabajadores.login(user, "miguel");
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(HOME_PAGE_URL);
		checkErrorMsg();
	}

	@Test
	// user seguido de espacios password correcto
	public void checkWrongLogin7() throws InterruptedException {

		LoginTrabajadores.login(user + "  ", pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(HOME_PAGE_URL);
		checkErrorMsg();
	}
	
	@Test
	@Ignore
	// user correcto password seguido de espacios
	public void checkWrongLogin8() throws InterruptedException {

		LoginTrabajadores.login(user, pssw + "  ");
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(HOME_PAGE_URL);
		checkErrorMsg();
	}

	@Test
	// user duplicado password correcto
	public void checkWrongLogin9() throws InterruptedException {

		LoginTrabajadores.login(user + user, pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(HOME_PAGE_URL);
		checkErrorMsg();
	}

	@Test
	// user correcto password duplicado
	public void checkWrongLogin10() throws InterruptedException {

		LoginTrabajadores.login(user, pssw + pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(HOME_PAGE_URL);
		checkErrorMsg();
	}

	@Test
	// user DNI false password correcto
	public void checkWrongLogin11() throws InterruptedException {

		LoginTrabajadores.login("07939829B", pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(HOME_PAGE_URL);
		checkErrorMsg();
	}

	@Test
	// sobrepasa numero de caracteres en user
	public void checkWrongLogin12() throws InterruptedException {
		String aux = "";
		for (int i = 0; i < 100; i++) {
			aux += user;
		}
		LoginTrabajadores.login(aux, pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(HOME_PAGE_URL);
		checkErrorMsg();
	}

	@Test
	// sobrepasa numero de caracteres en password
	public void checkWrongLogin13() throws InterruptedException {
		String aux = "";
		for (int i = 0; i < 100; i++) {
			aux += pssw;
		}
		LoginTrabajadores.login(user, aux);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(HOME_PAGE_URL);
		checkErrorMsg();
	}

	@Test
	// user correcto password en mayusculas
	public void checkWrongLogin14() throws InterruptedException {
		LoginTrabajadores.login(user, pssw.toUpperCase());

		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(HOME_PAGE_URL);
		checkErrorMsg();
	}

	@Test
	public void checkLiterals() {

		String titulo = LoginTrabajadores.getTitle();

		// Titulo
		assertNotNull(titulo);
		assertThat(titulo).isEqualTo("Inicio de sesión");
	}

	@Test
	public void checkTechsuiteLogo() {
		assertNotNull(
				LoginTrabajadores.getTechsuiteLogo("https://drive.google.com/uc?id=11S5p_sDmewpkVvXgnvdmj8SmkhX1RIJf"));
	}

	@Test
	public void checkNavBar() {

		List<String> literals = LoginTrabajadores.getLiterals();

		assertThat(literals.get(0)).isEqualTo("Inicio");
		assertThat(literals.get(1)).isEqualTo("Login");
	}

	@Test
	public void checkNavigation() {

		String raizUrl = "https://techsuite.gitlab.io/madrid";

		LoginTrabajadores.clickOnNavBar("Inicio ");
		assertThat(driver.getCurrentUrl()).isEqualTo(raizUrl + "/");

		LoginTrabajadores.clickOnNavBar("Login ");
		assertThat(driver.getCurrentUrl()).isEqualTo(raizUrl + "/login");
	}

}
