package app.clinica.madrid;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;

import app.GenericTest;

public class PagPacientesTest extends GenericTest {

	private static final String pag_pacientes = "https://techsuite.gitlab.io/madrid/login";
	private PagPacientes PagPacientes;
	private String user = "00000000v";
	private String pssw = "asdf";

	@Before
	public void pacientesSetup() throws InterruptedException {
		driver.get(pag_pacientes);
		PagPacientes = new PagPacientes(driver);
		PagPacientes.login(user, pssw);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	// Comprueba que al clicar en todos los botones se nos redirecciona
	// correctamente a la correspondiente página
	@Test
	public void checkNavigation() throws InterruptedException {

		String urlRaiz = "https://techsuite.gitlab.io/madrid/";

		PagPacientes.clickOnBotones("Pacientes");
		Thread.sleep(1000);
		assertThat(driver.getCurrentUrl()).isEqualTo(urlRaiz + "home/pacientes");

		PagPacientes.clickOnNavBar("Pacientes ");
		assertThat(driver.getCurrentUrl()).isEqualTo(urlRaiz + "home/pacientes");

		PagPacientes.clickOnNavBar("Inicio ");
		assertThat(driver.getCurrentUrl()).isEqualTo(urlRaiz + "home/bienvenida");

		driver.navigate().back();

		Thread.sleep(1000);

		PagPacientes.clickOnNavBar("Techsuite-Wiki ");
		assertThat(driver.getCurrentUrl()).isEqualTo(urlRaiz + "home/techsuite-wiki");

		driver.navigate().back();

		Thread.sleep(1000);

		PagPacientes.clickDesconecta();
		assertThat(driver.getCurrentUrl()).isEqualTo(urlRaiz);

	}

	@Test
	public void checkLiterals() throws InterruptedException {
		// Vamos a pacientes
		PagPacientes.clickOnBotones("Pacientes");
		Thread.sleep(1000);
		
		//Comprobar título
		String titulo=PagPacientes.getTitle();
		assertNotNull(titulo);
		assertThat(titulo).isEqualTo("Mis Pacientes");

		// Comprobamos botones clicando en ellos
		// Botones de idiomas
		PagPacientes.clickOnBotones("ENG ");
		Thread.sleep(500);
		PagPacientes.clickOnBotones("ESP ");
		Thread.sleep(500);

		// Botón de "Todos los pacientes"
		PagPacientes.clickOnBotones("Todos los pacientes ");
		Thread.sleep(500);
		// Si se clica en dicho botón, debería aparecer otro de flecha para navegar por
		// la lista de pacientes
		PagPacientes.clickOnFlecha("");
		Thread.sleep(500);
		
		// mediante el uso de numPagsPacientes sabemos cuantas páginas hay así podemos
		// ver que salgan todas
		int numPags = PagPacientes.numPagsPacientes();
		for(int i=2; i<numPags;i++) {
			PagPacientes.clickOnFlecha("[2]");
			Thread.sleep(200);
		}
		PagPacientes.clickOnFlecha("");
		Thread.sleep(500);
		
		for(int j=2;j<numPags;j++) {
			PagPacientes.clickOnFlecha("[1]");
			Thread.sleep(500);
		}
		
		// Comprobamos el botón de buscar
		PagPacientes.clickOnBuscar("enviarUnico");
		// Tras clicar debería aparecer el botón de cancelar
		PagPacientes.clickOnBuscar("borrarVarios");
		
		
		
	}
}
