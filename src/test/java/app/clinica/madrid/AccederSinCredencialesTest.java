package app.clinica.madrid;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import app.GenericTest;

public class AccederSinCredencialesTest extends GenericTest {

	// URL's que necesitarian credenciales previas
	private static final String pag_trabajadores = "https://techsuite.gitlab.io/madrid/home/bienvenida";
	private static final String pag_pacientes = "https://techsuite.gitlab.io/madrid/home/pacientes";
	private static final String pag_techsuite = "https://techsuite.gitlab.io/madrid/home/techsuite-wiki";
	private static final String pag_detalles_paciente = "https://techsuite.gitlab.io/madrid/home/pacientes/detalle";
	private static final String pag_techsuite_medicamentos = "https://techsuite.gitlab.io/madrid/home/techsuite-wiki/medicamento";
	private static final String pag_techsuite_comparaciones = "https://techsuite.gitlab.io/madrid/home/techsuite-wiki/comparacion";
	private static final String[] pags = { pag_trabajadores, pag_pacientes, pag_techsuite, pag_detalles_paciente,
			pag_techsuite_medicamentos, pag_techsuite_comparaciones };

	// URL del login
	private static final String pag_login = "https://techsuite.gitlab.io/madrid/login";

	@Test
	public void intentaAcceder() throws InterruptedException {

		for (int i = 0; i < pags.length; i++) {
			driver.get(pags[i]);
			Thread.sleep(500);
			assertThat(driver.getCurrentUrl()).isEqualTo(pag_login);
		}
	}

}
