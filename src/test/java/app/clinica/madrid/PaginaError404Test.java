package app.clinica.madrid;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import app.GenericTest;
import app.clinica.dakar.Helper;

public class PaginaError404Test extends GenericTest {

	PaginaError404 PaginaError404;

	@Before
	public void PaginaError404Setup() {
		driver.get("https://www.google.com/");
		PaginaError404 = new PaginaError404(driver);
		Helper.init(driver);
	}

	/*
	 * Comprobamos aquellas paginas que deberian llevarnos a la pagina de error 404
	 */
	@Test
	public void redirectsTo404() {
		List<String> urls = new ArrayList<>();
		urls.add("http://techsuite.gitlab.io/madrid/logan");
		urls.add("http://techsuite.gitlab.io/madrid/home/pacentes");
		urls.add("http://techsuite.gitlab.io/madrid/hdhd");
		urls.add("http://techsuite.gitlab.io/madrid/home/2021");
		urls.add("http://techsuite.gitlab.io/madrid/home/pacientes/login");
		urls.add("http://techsuite.gitlab.io/madrid/askjdbflkqbgajslnvqg15151");
		urls.add("http://techsuite.gitlab.io/madrid/techsuite-wiki");
		urls.add("http://techsuite.gitlab.io/madrid/bienvenida");
		urls.add("http://techsuite.gitlab.io/madrid/login/home");

		// 10 urls aleatorias
		for (int i = 0; i < 10; i++) {
			urls.add("http://techsuite.gitlab.io/madrid/" + UUID.randomUUID().toString().replace("-", ""));
		}

		for (String url : urls) {
			driver.get(url);
			assertTrue(PaginaError404.inPaginaError404());
			assertTrue(redireccionFunciona());
		}

	}

	private boolean redireccionFunciona() {
		boolean res = true;
		String prevUrl = driver.getCurrentUrl();
		PaginaError404.getButton().click();
		try {
			Helper.fluentUrlChanged(prevUrl);
			String actual = driver.getCurrentUrl();
			res = actual.equals("http://techsuite.gitlab.io/madrid/");
		} catch (Exception e) {
			res = false;
		}
		return res;
	}
}
