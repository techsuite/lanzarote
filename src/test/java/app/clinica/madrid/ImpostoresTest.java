package app.clinica.madrid;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import app.GenericTest;

public class ImpostoresTest extends GenericTest {

	private static final String pag_login = "https://techsuite.gitlab.io/madrid/login";
	private Impostores Impostores;
	private String user = "89328314X";
	private String pssw = "contra123";
	private String[] claves = { "42738", "12734", "12345" };

	@Before
	public void impostoresSetup() throws InterruptedException {
		driver.get(pag_login);
		Impostores = new Impostores(driver);
		Impostores.login(user, pssw);
		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	
	@Test
	public void checkKeys() throws InterruptedException {

		// Clave 1 (Doctor)
		Impostores.loginClave(claves[0]);
		Thread.sleep(1000);
		// Comprueba que se ha accedido correctamente
		assertThat(driver.getCurrentUrl()).isEqualTo("https://techsuite.gitlab.io/madrid/home/bienvenida");
		String icono = Impostores.getIcono();
		Thread.sleep(1000);
		assertThat(icono).isEqualTo("https://images.emojiterra.com/google/android-11/512px/1fa7a.png");

		// Clave 2 (Doctor)
		loginAgain(claves[1]);
		Thread.sleep(500);
		// Comprueba que se ha accedido correctamente
		assertThat(driver.getCurrentUrl()).isEqualTo("https://techsuite.gitlab.io/madrid/home/bienvenida");
		String icono1 = Impostores.getIcono();
		Thread.sleep(1000);
		assertThat(icono1).isEqualTo("https://images.emojiterra.com/google/android-11/512px/1fa7a.png");

		// Clave 3 (Laboratorio)
		loginAgain(claves[2]);
		Thread.sleep(500);
		// Comprueba que se ha accedido correctamente
		assertThat(driver.getCurrentUrl()).isEqualTo("https://techsuite.gitlab.io/madrid/home/bienvenida");
		String icono2 = Impostores.getIcono();
		Thread.sleep(1000);
		assertThat(icono2).isEqualTo("https://images.emojiterra.com/twitter/v13.0/512px/1f52c.png");
	}

	@Test
	public void checkNavigation() throws InterruptedException {
		String urlRaiz = "https://techsuite.gitlab.io/madrid/";

		Impostores.clickOnNavBar("Inicio ");
		Thread.sleep(500);
		assertThat(driver.getCurrentUrl()).isEqualTo(urlRaiz);

		driver.navigate().back();

		Impostores.clickOnNavBar("Login ");
		Thread.sleep(500);
		assertThat(driver.getCurrentUrl()).isEqualTo(urlRaiz + "login");

		Impostores.clickDesconecta2();
		assertThat(driver.getCurrentUrl()).isEqualTo(urlRaiz);
	}

	// De momento, la única diferencia entre doctores y personal de laboratorio es
	// icono encima de "bienvenid@"
	@Test
	public void checkLiterals() throws InterruptedException {
		String texto = Impostores.getTitle();
		assertThat(texto).isEqualTo("Inicio de sesión");

		// Es importante comprobar, que al iniciar sesión con impostor, el botón de
		// entrar pasa a ser "enviar" como lo pone en los acuerdos.
		
		String enviar = Impostores.getButtonText();
		assertThat(enviar).isEqualTo("Enviar");
		
		// Comprobamos el texto del input de la clave
		String texto1 = Impostores.getInputText();
		assertThat(texto1).isEqualTo("Clave doctor a simular");
		
	}

	//Usada para desconectar y volver a acceder con otra clave
	public void loginAgain(String clave) throws InterruptedException {

		Impostores.clickDesconecta1();
		Thread.sleep(1000);
		Impostores.clickInicia();
		Thread.sleep(1000);
		Impostores.login(user, pssw);
		Thread.sleep(1000);
		Impostores.loginClave(clave);
		Thread.sleep(500);

	}

}
