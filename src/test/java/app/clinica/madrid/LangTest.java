package app.clinica.madrid;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.Duration;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.GenericTest;

public class LangTest extends GenericTest {

	private WebStrings WebStrings;
	private static final String LOGIN_URL = "https://techsuite.gitlab.io/madrid/login";
	private static final String INITIAL_URL = "https://techsuite.gitlab.io/madrid/";
	private static final String HOME_URL = "https://techsuite.gitlab.io/madrid/home/bienvenida";
	private static final String PACIENTES_URL = "https://techsuite.gitlab.io/madrid/home/pacientes";
	private static final String WIKI_URL = "https://techsuite.gitlab.io/madrid/home/techsuite-wiki";
	
	@Before
	public void ServicesSetup() {
		driver.get(INITIAL_URL);
		WebStrings = new WebStrings(driver);
	}
	
	@Test
	public void testLangInicio() throws InterruptedException {
		testingChanges();
	}
	
	@Test
	public void testLangBienvenida() throws InterruptedException {
		driver.get(LOGIN_URL);
		WebStrings.login("00000000v", "asdf");
		new WebDriverWait(driver, Duration.ofSeconds(5))
			.until(driver -> driver.getCurrentUrl().equals(HOME_URL));
		Thread.sleep(1000);
		testingChanges();
	}
	
	@Test
	public void testLangPacientes() throws InterruptedException {
		driver.get(LOGIN_URL);
		WebStrings.login("00000000v", "asdf");
		new WebDriverWait(driver, Duration.ofSeconds(5))
			.until(driver -> driver.getCurrentUrl().equals(HOME_URL));
		driver.get(PACIENTES_URL);
		Thread.sleep(1000);
		testingChanges();
	}
	
	@Test
	public void testLangWiki() throws InterruptedException {
		driver.get(LOGIN_URL);
		WebStrings.login("00000000v", "asdf");
		new WebDriverWait(driver, Duration.ofSeconds(5))
			.until(driver -> driver.getCurrentUrl().equals(HOME_URL));
		driver.get(WIKI_URL);
		Thread.sleep(1000);
		testingChanges();
	}
	
	private void testingChanges() throws InterruptedException {
		List<String> actualStrings = WebStrings.getAllStrings();
		WebStrings.changeLang("EN");
		List<String> translatedStrings = WebStrings.getAllStrings();
		checkIntegrity(actualStrings, translatedStrings, 0);
	}
	
	private void checkIntegrity(List<String> prev, List<String> post, double ratio) {
		int diferencias = 0;
		// Comprobamos que no se haya perdido ningun String
		assertEquals(prev.size(), post.size());
		// Comprobamos que cambian al menos un 10% de los strings (para tener en cuenta
		// los nombres propios)
		for (int i = 0; i < prev.size(); i++) {
			if (!prev.get(i).equals(post.get(i)))
				diferencias++;
		}
		assertTrue((double) diferencias >= ratio * prev.size());
	}
	
}
