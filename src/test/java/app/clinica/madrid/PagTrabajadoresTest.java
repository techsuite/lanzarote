package app.clinica.madrid;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;

import app.GenericTest;

public class PagTrabajadoresTest extends GenericTest {

	private static final String pag_trabajadores = "https://techsuite.gitlab.io/madrid/login";
	private PagTrabajadores PagTrabajadores;
	private String user = "00000000v";
	private String pssw = "asdf"; 

	@Before
	public void trabajadoresSetup() throws InterruptedException {
		driver.get(pag_trabajadores);
		PagTrabajadores = new PagTrabajadores(driver);
		PagTrabajadores.login(user, pssw);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
	}
	
	@Test	
	public void checkLiterals() throws InterruptedException {
		//Comprueba el título de bienvenida, aquí "Bienvenido" ya que el login está hecho con Miguel.
		String titulo = PagTrabajadores.getTitle();
		Thread.sleep(1000);
		assertNotNull(titulo);
		assertThat(titulo).isEqualTo("Bienvenido");
		
		//Comprobación de existencia y de imagen correcta de los iconos.
		//Logo de Techsuite.
		
		String logo = PagTrabajadores.getIconos("Logo");
		Thread.sleep(1000);
		assertNotNull(logo);
		assertThat(logo).isEqualTo("https://drive.google.com/uc?id=11S5p_sDmewpkVvXgnvdmj8SmkhX1RIJf");
		
		//Icono de bienvenida "foto-doctor".
		String foto_doctor = PagTrabajadores.getIconos("Saludos");
		Thread.sleep(1000);
		assertNotNull(foto_doctor);
		assertThat(foto_doctor).isEqualTo("https://images.emojiterra.com/google/android-11/512px/1fa7a.png");
		
		//Comprobación de existencia de botones de español e ingles. De momento solo se clica, si los botones no están, saltará error.
		PagTrabajadores.clickOnBotones("ENG ");
		PagTrabajadores.clickOnBotones("ESP ");
		
	}
		
	//Comprueba que al clicar en todos los botones se nos redirecciona correctamente a la correspondiente página
    @Test 	
	public void checkNavigation() throws InterruptedException {

		String urlRaiz = "https://techsuite.gitlab.io/madrid/";
		
		PagTrabajadores.clickOnNavBar("Inicio ");
		assertThat(driver.getCurrentUrl()).isEqualTo(urlRaiz );
		
		driver.navigate().back();
		
		Thread.sleep(500);
		
		PagTrabajadores.clickOnBotones("Pacientes");
		Thread.sleep(1000);
		assertThat(driver.getCurrentUrl()).isEqualTo(urlRaiz + "home/pacientes");

		driver.navigate().back();
		
		PagTrabajadores.clickOnBotones("Techsuite-Wiki");
		assertThat(driver.getCurrentUrl()).isEqualTo(urlRaiz + "home/techsuite-wiki");
		
		driver.navigate().back();	
	
		PagTrabajadores.clickOnNavBar("Pacientes ");
		assertThat(driver.getCurrentUrl()).isEqualTo(urlRaiz + "home/pacientes");
		
		driver.navigate().back();
		
		PagTrabajadores.clickOnNavBar("Techsuite-Wiki ");
		assertThat(driver.getCurrentUrl()).isEqualTo(urlRaiz + "home/techsuite-wiki");
		
		driver.navigate().back();	
		
		PagTrabajadores.clickDesconecta();
		assertThat(driver.getCurrentUrl()).isEqualTo("https://techsuite.gitlab.io/madrid/");
		
		
	}

}
