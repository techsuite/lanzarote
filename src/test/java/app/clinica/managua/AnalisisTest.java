package app.clinica.managua;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import app.GenericTest;

public class AnalisisTest extends GenericTest {

	private static final String PAGE = "https://techsuite.gitlab.io/managua-fe/";
	private PaginaBusquedaPacientes PaginaBusquedaPacientes;
	private PaginaAnalisis PaginaAnalisis;

	@Before
	public void AnalisisTestSetup() throws InterruptedException {
		driver.manage().window().maximize();
		driver.get(PAGE);
		PaginaAnalisis = new PaginaAnalisis(driver);
		PaginaBusquedaPacientes = new PaginaBusquedaPacientes(driver);
		Helper.init(driver);
		String prev = driver.getCurrentUrl();
		Helper.login("12345678N", "1111");
		Helper.fluentUrlChanged(prev);
		moveToAnalisis();
	}

	private void moveToAnalisis() {
		String prev = driver.getCurrentUrl();
		Helper.fluentElement("//button[text()='Análisis de Sangre']").click();
		Helper.fluentUrlChanged(prev);
	}

	@Test
	public void testAddPrueba() throws InterruptedException {
		// Obtenemos todos los pacientes que queramos con algun filtro
		Helper.fluentElement("//button[@id='addAnalisis']").click();
		Thread.sleep(500);
		PaginaBusquedaPacientes.filtrar("nombre", "David"); // David por ejemplo

		// Añadimos analisis para estos pacientes
		int nPruebas = PaginaBusquedaPacientes.iniciarTodasPruebas();

		// Retrocedemos y comprobamos que todos los analisis esten en proceso
		PaginaBusquedaPacientes.volver();

		// Esperamos que los analisis salgan de la cola
		Thread.sleep(15000);

		// assertFalse(PaginaAnalisis.isTablaVacia());

		// Tomamos los tiempos restantes y cogemos el mayor (tener en cuenta que está en
		// centesimas)
		List<Integer> tiempos = PaginaAnalisis.getAnalisis().stream().map(x -> Integer.parseInt(x.get(3)))
				.collect(Collectors.toList());
		List<String> ids = PaginaAnalisis.getAnalisis().stream().map(x -> x.get(0)).collect(Collectors.toList());
		int maximo = Collections.max(tiempos);

		Thread.sleep(maximo * 1000 + 2000);

		// Esperamos hasta que acabe el último y verificamos que ahora este vacía la
		// tabla
		assertTrue(PaginaAnalisis.isTablaVacia());

		// Comprobamos ahora que esté en la tabla de finalizados
		Thread.sleep(2000);
		List<String> idsFinalizados = PaginaAnalisis.getFinalizados();
		assertTrue(idsFinalizados.containsAll(ids));
	}

	@Test
	public void testRemovePrueba() throws InterruptedException {

		// Obtenemos todos los pacientes que queramos con algun filtro
		Helper.fluentElement("//button[@id='addAnalisis']").click();
		Thread.sleep(500);
		PaginaBusquedaPacientes.filtrar("nombre", "David"); // David por ejemplo

		// Añadimos analisis para este pacientee
		PaginaBusquedaPacientes.iniciarUnaPrueba();

		// Retrocedemos y obtenemos el id del analisis
		PaginaBusquedaPacientes.volver();

		// Esperamos que los analisis salgan de la cola
		Thread.sleep(15000);

		// Hacemos click en eliminar e introducimos el id
		String id = PaginaAnalisis.getAnalisis().get(0).get(0);
		PaginaAnalisis.removeAnalisis(id);

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}

		// Comprobamos que ya no esté
		assertTrue(PaginaAnalisis.isTablaVacia());
	}
}
