package app.clinica.managua;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import app.GenericTest;

public class AccederSinCredencialesTest extends GenericTest {

	private static final String login_url = "https://techsuite.gitlab.io/managua-fe/";
	private static final String welcome_url = "https://techsuite.gitlab.io/managua-fe/acceso/welcome";
	private static final String ansangre_url = "https://techsuite.gitlab.io/managua-fe/acceso/ctrlPanel";
	private static final String pcr_url = "https://techsuite.gitlab.io/managua-fe/acceso/ctrlPanelPCR";
	private static final String buscarpac_url = "https://techsuite.gitlab.io/managua-fe/acceso/search";
	private static final String nuevoingr_url = "https://techsuite.gitlab.io/managua-fe/acceso/search/formulario";

	static final String[] pags = { welcome_url, ansangre_url, pcr_url, buscarpac_url, nuevoingr_url };

	@Test
	public void intentaAcceder() {
		for (int i = 0; i < pags.length; i++) {
			driver.get(pags[i]);
			assertThat(driver.getCurrentUrl()).isEqualTo(login_url);
		}
	}

}
