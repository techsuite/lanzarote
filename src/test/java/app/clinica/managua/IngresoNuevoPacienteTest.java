package app.clinica.managua;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.time.Duration;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.GenericTest;

public class IngresoNuevoPacienteTest extends GenericTest {

	private IngresoNuevoPaciente IngNewPaciente;
	private static final String DNI = "11345467W";
	private static final String PASS = "1234";
	private static final String LOGIN_URL = "https://techsuite.gitlab.io/managua-fe/";
	private static final String WELCOME_URL = "https://techsuite.gitlab.io/managua-fe/acceso/welcome";
	private static final String ANALISIS_URL = "https://techsuite.gitlab.io/managua-fe/acceso/ctrlPanel";
	private static final String NUEVO_PACIENTE_URL = "https://techsuite.gitlab.io/managua-fe/acceso/search/formulario";
	private static final String RETURN_PAGE_URL = "https://techsuite.gitlab.io/managua-fe/acceso/search";
	
	private static final String DNI_LABEL = "DNI: ";
	private static final String NOMBRE_LABEL = "Nombre: ";
	private static final String APELLIDOS_LABEL = "Apellidos: ";
	private static final String CORREO_LABEL = "Correo Electrónico: ";
	private static final String NACIMIENTO_LABEL = "Fecha de nacimiento: ";
	
	@Before
	public void ingresoNuevoPacienteSetUp () {
		IngNewPaciente = new IngresoNuevoPaciente(driver);
		driver.get(LOGIN_URL);
		Helper.init(driver);
		Helper.login(DNI, PASS);
		assertThat(driver.getCurrentUrl()).isEqualTo(WELCOME_URL);
		new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Análisis de Sangre']")));
		Helper.fluentElement("//button[text()='Análisis de Sangre']").click();
		assertThat(driver.getCurrentUrl()).isEqualTo(ANALISIS_URL);
		new WebDriverWait(driver, Duration.ofSeconds(5))
			.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@id='addAnalisis']")));
		Helper.fluentElement("//button[@id='addAnalisis']").click();
		assertThat(driver.getCurrentUrl()).isEqualTo(RETURN_PAGE_URL);
		new WebDriverWait(driver, Duration.ofSeconds(5))
			.until(ExpectedConditions.elementToBeClickable(By.xpath("//button/b[text()='Paciente de nuevo ingreso']")));
		Helper.fluentElement("//button/b[text()='Paciente de nuevo ingreso']").click();
		assertThat(driver.getCurrentUrl()).isEqualTo(NUEVO_PACIENTE_URL);
	}
	
	
	/*	Este test realiza las siguientes aserciones agrupadas:
	 * 	1- Textos superiores
	 * 	2- Textos a la izquierda de los inputs
	 * 	3- Inputs donde se puede escribir (y su id es el esperado?)
	 * 	4- El boton de insercion esta desactivado
	 * 	5- El boton de insercion esta activado si existen datos en los inputs
	 */
	@Test
	public void checkLiterals(){
		
		assertThat(IngNewPaciente.getReturnButtonText()).isEqualTo("Volver sin guardar");
		assertThat(IngNewPaciente.getTitle()).isEqualTo("Datos para el paciente a ingresar");
		
		assertThat(IngNewPaciente.getLabelText(DNI_LABEL)).isEqualTo("DNI:");
		assertThat(IngNewPaciente.getLabelText(NOMBRE_LABEL)).isEqualTo("Nombre:");
		assertThat(IngNewPaciente.getLabelText(APELLIDOS_LABEL)).isEqualTo("Apellidos:");
		assertThat(IngNewPaciente.getLabelText(CORREO_LABEL)).isEqualTo("Correo Electrónico:");
		assertThat(IngNewPaciente.getLabelText(NACIMIENTO_LABEL)).isEqualTo("Fecha de nacimiento:");
		
		WebElement dniInput = IngNewPaciente.getInput(DNI_LABEL);
		assertTrue(dniInput.isEnabled());
		//assertThat(dniInput.getAttribute("formcontrolname")).isEqualTo("dni");
		assertThat(dniInput.getAttribute("placeholder")).isEqualTo("ej: 12345678A");
		WebElement nombreInput = IngNewPaciente.getInput(NOMBRE_LABEL);
		assertTrue(nombreInput.isEnabled());
		//assertThat(nombreInput.getAttribute("formcontrolname")).isEqualTo("nombre");
		assertThat(nombreInput.getAttribute("placeholder")).isEqualTo("p. ej: Sara");
		WebElement apellidosInput = IngNewPaciente.getInput(APELLIDOS_LABEL);
		assertTrue(apellidosInput.isEnabled());
		//assertThat(apellidosInput.getAttribute("formcontrolname")).isEqualTo("apellidos");
		assertThat(apellidosInput.getAttribute("placeholder")).isEqualTo("p. ej: Gonzalez Pérez");
		WebElement correoInput = IngNewPaciente.getInput(CORREO_LABEL);
		assertTrue(correoInput.isEnabled());
		//assertThat(correoInput.getAttribute("formcontrolname")).isEqualTo("correo");
		assertThat(correoInput.getAttribute("placeholder")).isEqualTo("p. ej: sara.gonzalez@gmail.com");
		WebElement nacimientoInput = IngNewPaciente.getInput(NACIMIENTO_LABEL);
		assertTrue(nacimientoInput.isEnabled());
		//assertThat(nacimientoInput.getAttribute("formcontrolname")).isEqualTo("nacimiento");
		assertThat(nacimientoInput.getAttribute("placeholder")).isEqualTo("ej: 2021-04-12");
		WebElement generoInput = IngNewPaciente.getGenderSelection();
		assertTrue(generoInput.isEnabled());
		IngNewPaciente.getGenderOption("Hombre").click();
		//assertThat(generoInput.getAttribute("formcontrolname")).isEqualTo("genero");
		
		WebElement botonInsertar = IngNewPaciente.getInsertButton();
		WebElement botonInsertarYEmpezar = IngNewPaciente.getInsertAndStartButton();
		
		assertTrue(!botonInsertar.isEnabled());
		assertTrue(!botonInsertarYEmpezar.isEnabled());
		
		dniInput.sendKeys("a");
		nombreInput.sendKeys("a");
		apellidosInput.sendKeys("a");
		correoInput.sendKeys("a");
		nacimientoInput.sendKeys("a");
		IngNewPaciente.getGenderOption("Mujer").click();
		
		assertTrue(botonInsertar.isEnabled());
		assertTrue(botonInsertarYEmpezar.isEnabled());
		
	}
	
	@Test
	public void checkClickables() {
		boolean successfulOperation = true;
		try {
			IngNewPaciente.getReturnButton().click();
			Thread.sleep(500);
			successfulOperation = new WebDriverWait(driver, Duration.ofSeconds(5))
					.until(driver -> driver.getCurrentUrl().equals(RETURN_PAGE_URL));
		}catch(Exception e) {
			successfulOperation = false;
		}
		assertTrue(successfulOperation);
		
		//A espera de que se realice el boton de desconectarse
		/*
		driver.get(NUEVO_PACIENTE_URL);

		try {
			
			new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(driver -> IngNewPaciente.getLogoutButton()!=null);
			
			WebElement boton = IngNewPaciente.getLogoutButton();
			
			boton.click();
			
			successfulOperation = new WebDriverWait(driver, Duration.ofSeconds(5))
					.until(driver -> driver.getCurrentUrl().equals(LOGIN_URL));
		}catch(TimeoutException e) {
			successfulOperation = false;
		}
		
		assertTrue(successfulOperation);
		*/
	}
	
}
