package app.clinica.managua;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import app.GenericTest;

public class BusquedaPacientesTest extends GenericTest {

	private static final String PAGE = "https://techsuite.gitlab.io/managua-fe/";
	private PaginaBusquedaPacientes PaginaBusquedaPacientes;

	@Before
	public void BusquedaPacientesTestSetup() {
		driver.get(PAGE);
		PaginaBusquedaPacientes = new PaginaBusquedaPacientes(driver);
		Helper.init(driver);
		Helper.login("12345678N", "1111");
		moveToBusqueda();
	}

	private void moveToBusqueda() {
		String prev = driver.getCurrentUrl();
		Helper.fluentElement("//button[text()='Análisis de Sangre']").click();
		Helper.fluentUrlChanged(prev);
		Helper.fluentElement("//button[@id='addAnalisis']").click();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
		}
	}

	@Test
	public void testBusquedaDNI() {
		List<String> dnis = Arrays.asList("11111111W", "12345678W", "44578745E");
		for (String dni : dnis) {
			PaginaBusquedaPacientes.filtrar("dni", dni);
			List<String> resultadoBusqueda = PaginaBusquedaPacientes.getDnis();
			assertTrue(resultadoBusqueda.stream().allMatch(x -> x.equals(dni)));
		}
	}

	@Test
	public void testBusquedaNombre() {
		List<String> nombres = Arrays.asList("David");
		for (String nombre : nombres) {
			PaginaBusquedaPacientes.filtrar("nombre", nombre);
			Helper.scrollToBottom();
			List<String> resultadoBusqueda = PaginaBusquedaPacientes.getNombres();
			assertTrue(resultadoBusqueda.stream().allMatch(x -> x.equals(nombre)));
		}
	}

	@Test

	public void testBusquedaApellidos() {
		List<String> apellidoss = Arrays.asList("Vinas Viruta", "Gonzalez", "Gutierrez Alcaide");
		for (String apellidos : apellidoss) {
			PaginaBusquedaPacientes.filtrar("apellidos", apellidos);
			List<String> resultadoBusqueda = PaginaBusquedaPacientes.getApellidos();
			assertTrue(resultadoBusqueda.stream().allMatch(x -> x.contains(apellidos)));
		}

	}
}
