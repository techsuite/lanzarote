package app.clinica.managua;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.time.Duration;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.GenericTest;

public class LoginManaguaTest extends GenericTest {

	private LoginManagua LoginManagua;
	private static final String DNI = "11345467W";
	private static final String PASS = "1234";
	private static final String DNI2 = "11111111Z";
	private static final String PASS2 = "ciencia1";
	private static final String DNI3 = "99999999E";
	private static final String PASS3 = "Noche5";
	private static final String DNI4 = "12345678Z";
	private static final String PASS4 = "ocho888";
	private static final String DNI5 = "12345678N";
	private static final String PASS5 = "1111";
	
	//TODO
	//Cambiar al texto determinado por los acuerdos para cuando estén implementados
	private static final String DNI_TEXT = "DNI:";
	private static final String PASSWORD_TEXT = "Password:";
	private static final String LOGIN_PAGE_URL = "https://techsuite.gitlab.io/managua-fe/";
	private static final String POSTLOGIN_PAGE_URL = "https://techsuite.gitlab.io/managua-fe/acceso/welcome";
	
	@Before
	public void LoginManaguaSetup() {
		driver.get(LOGIN_PAGE_URL);
		LoginManagua = new LoginManagua(driver);
	}
	
	@Test
	public void checkDNIText() {
		assertThat(LoginManagua.getTextLoginDNI()).isEqualTo(DNI_TEXT);
	}
	
	@Test
	public void checkPasswordText() {
		assertThat(LoginManagua.getTextLoginPassword()).isEqualTo(PASSWORD_TEXT);
	}
	
	@Test
	public void successfulLogin1() {
		LoginManagua.login(DNI, PASS);
		boolean successfulOperation;
		successfulOperation = new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(driver -> driver.getCurrentUrl().equals(POSTLOGIN_PAGE_URL));
		assertThat(successfulOperation);
	}
	
	@Ignore
	@Test
	public void successfulLogin2() throws InterruptedException {
		LoginManagua.login(DNI2, PASS2);
		boolean successfulOperation;
		successfulOperation = new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(driver -> driver.getCurrentUrl().equals(POSTLOGIN_PAGE_URL));
		assertThat(successfulOperation);
	}
	
	@Test
	public void successfulLogin3() throws InterruptedException {
		LoginManagua.login(DNI3, PASS3);
		boolean successfulOperation;
		successfulOperation = new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(driver -> driver.getCurrentUrl().equals(POSTLOGIN_PAGE_URL));
		assertThat(successfulOperation);
	}
	
	@Test
	public void successfulLogin4() throws InterruptedException {
		LoginManagua.login(DNI4, PASS4);
		boolean successfulOperation;
		successfulOperation = new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(driver -> driver.getCurrentUrl().equals(POSTLOGIN_PAGE_URL));
		assertThat(successfulOperation);
	}
	
	@Test
	public void successfulLogin5() throws InterruptedException {
		LoginManagua.login(DNI5, PASS5);
		boolean successfulOperation;
		successfulOperation = new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(driver -> driver.getCurrentUrl().equals(POSTLOGIN_PAGE_URL));
		assertThat(successfulOperation);
	}
	
	//Se repiten los mismos test en orden de login como en Madrid y Dakar
	
	@Test
	// password vacio dni correcto
	public void wrongLogin1() {
		LoginManagua.login(DNI, "");
		assertTrue(LoginManagua.catchErrorMessage(LOGIN_PAGE_URL));
	}
	
	@Test
	// dni vacio password correcto
	public void wrongLogin2() {
		LoginManagua.login("", PASS);
		assertTrue(LoginManagua.catchErrorMessage(LOGIN_PAGE_URL));
	}
	
	@Test
	// dni espacios password correcto
	public void wrongLogin3() {
		LoginManagua.login("      ", PASS);
		assertTrue(LoginManagua.catchErrorMessage(LOGIN_PAGE_URL));
	}
	
	@Test
	// dni correcto password espacios
	public void wrongLogin4() {
		LoginManagua.login(DNI, "      ");
		assertTrue(LoginManagua.catchErrorMessage(LOGIN_PAGE_URL));
	}
	
	@Test
	// dni String aleatorio password correcto
	public void wrongLogin5() {
		LoginManagua.login("Juan", PASS);
		assertTrue(LoginManagua.catchErrorMessage(LOGIN_PAGE_URL));
	}
	
	@Test
	// dni correcto password String aleatorio
	public void wrongLogin6() {
		LoginManagua.login(DNI, "Juan");
		assertTrue(LoginManagua.catchErrorMessage(LOGIN_PAGE_URL));
	}
	
	@Test
	// dni seguido de espacios password correcto
	public void wrongLogin7() {
		LoginManagua.login(DNI + "  ", PASS);
		assertTrue(LoginManagua.catchErrorMessage(LOGIN_PAGE_URL));
	}
	
	@Test
	@Ignore
	// dni correcto password seguido de espacios
	public void wrongLogin8() {
		LoginManagua.login(DNI, PASS + "  ");
		assertTrue(LoginManagua.catchErrorMessage(LOGIN_PAGE_URL));
	}
	
	@Test
	// dni duplicado password correcto
	public void wrongLogin9() {
		LoginManagua.login(DNI + DNI, PASS);
		assertTrue(LoginManagua.catchErrorMessage(LOGIN_PAGE_URL));
	}
	
	@Test
	// dni correcto password duplicado
	public void wrongLogin10() {
		LoginManagua.login(DNI, PASS + PASS);
		assertTrue(LoginManagua.catchErrorMessage(LOGIN_PAGE_URL));
	}
	
	@Test
	// user DNI false password correcto
	public void wrongLogin11() {
		LoginManagua.login(DNI2, PASS);
		assertTrue(LoginManagua.catchErrorMessage(LOGIN_PAGE_URL));
	}
	
	@Test
	// sobrepasa numero de caracteres en dni
	public void wrongLogin12() {
		String aux = "";
		for (int i = 0; i < 100; i++) {
			aux += DNI;
		}
		LoginManagua.login(aux, PASS);
		assertTrue(LoginManagua.catchErrorMessage(LOGIN_PAGE_URL));
	}
	
	@Test
	// sobrepasa numero de caracteres en password
	public void wrongLogin13() {
		String aux = "";
		for (int i = 0; i < 100; i++) {
			aux += PASS;
		}
		LoginManagua.login(DNI, aux);
		assertTrue(LoginManagua.catchErrorMessage(LOGIN_PAGE_URL));
	}
	
	@Test
	// dni correcto password en mayusculas
	public void wrongLogin14() {
		LoginManagua.login(DNI2, PASS2.toUpperCase());
		assertTrue(LoginManagua.catchErrorMessage(LOGIN_PAGE_URL));
	}
	
}
