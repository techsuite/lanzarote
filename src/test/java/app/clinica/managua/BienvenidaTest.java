package app.clinica.managua;

import static org.junit.Assert.assertNotNull;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import app.GenericTest;

public class BienvenidaTest extends GenericTest{

	private Bienvenida Bienvenida;
	private static final String PAGE_URL = "https://techsuite.gitlab.io/managua-fe/acceso/welcome";
	private static final String raiz_URL = "https://techsuite.gitlab.io/managua-fe/";

	@Before
	public void BienvenidaTestSetup() throws InterruptedException {
		driver.get(raiz_URL);
		Bienvenida = new Bienvenida(driver);
		Bienvenida.login("11345467W","1234");
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(PAGE_URL);
	}
	
	@Test
	public void checkLiterals() {
		String titulo = Bienvenida.getTitle();
		
		assertNotNull(titulo);
		assertThat(titulo).contains("Saludos");
	}
	
	@Test
	public void checkDesconectarseButton() {

		Bienvenida.clickOn("Desconectarse");
		assertThat(driver.getCurrentUrl()).isEqualTo(raiz_URL);

	}
	
	@Test
	public void checkNavAnalisis() throws InterruptedException {

		Bienvenida.clickOn("Análisis de Sangre");
		assertThat(driver.getCurrentUrl()).isEqualTo(raiz_URL + "acceso/ctrlPanel");
	}
	
	@Test
	public void checkNavPcr() throws InterruptedException {

		Bienvenida.clickOn("Pruebas PCR");
		assertThat(driver.getCurrentUrl()).isEqualTo(raiz_URL + "acceso/ctrlPanelPCR");
	}
	
	
}
