package app.clinica.testIntegros;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import app.GenericTest;
import app.clinica.madrid.LoginTrabajadores;

public class IntegridadPacientesTest extends GenericTest {

	private static final String DNI = "00000000v";
	private static final String PASS = "asdf";

	private static IntegridadPacientes IntegroPacient;
	private LoginTrabajadores LoginTrabajadores;
	
	private static final String HOME_PAGE_URL = "https://techsuite.gitlab.io/madrid/login";
	private static String BOSTON_LOGIN_URL = "https://techsuite.gitlab.io/boston-fe/login";

	@Test
	public void BostonCheckTest() throws InterruptedException {
		driver.get(BOSTON_LOGIN_URL);
		IntegroPacient = new IntegridadPacientes(driver);
		app.clinica.boston.Helper.init(driver);
		app.clinica.boston.Helper.login(DNI, PASS);
		
		Thread.sleep(4000);
		
		List<List<String>> list = new ArrayList<>();
		int tableLength;
		WebElement nextPageButton = IntegroPacient.getNextPageButton();
		while(nextPageButton!=null) {
			
			Thread.sleep(1000);
			tableLength = IntegroPacient.getTableLength();
			for(int i = 0; i < tableLength; i++)
				if(IntegroPacient.checkValidData(i,DNI))
					list.add(IntegroPacient.getTableEntry(i));
			
			nextPageButton.click();
			Thread.sleep(2000);
			nextPageButton = IntegroPacient.getNextPageButton();
		}
		tableLength = IntegroPacient.getTableLength();
		for(int i = 0; i < tableLength; i++)
			if(IntegroPacient.checkValidData(i,DNI))
				list.add(IntegroPacient.getTableEntry(i));
		Thread.sleep(2000);
		driver.get(HOME_PAGE_URL);
		LoginTrabajadores = new LoginTrabajadores(driver);
		LoginTrabajadores.login(DNI, PASS);
		Thread.sleep(2000);
		LoginTrabajadores.clickOn("Pacientes");
		Thread.sleep(4000);
		for(int i = 0; i<list.size(); i++) {
			List<String> list2 = list.get(i);
				//DNI
				String datoDni = list2.get(0);
				String datoNombre = list2.get(1);
				String datoGenero = list2.get(3);
				checkDni(datoDni, datoNombre, datoGenero);
		}
	}
	
	public void checkDni(String datoDni, String datoNombre, String datoGenero) throws InterruptedException {
		driver.findElement(By.xpath("//select")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//option[text()='DNI']")).click();
		Thread.sleep(500);
		WebElement buscaDni = driver.findElement(By.xpath("//input[@type='text']"));
		buscaDni.clear();
		buscaDni.sendKeys(datoDni);
		Thread.sleep(500);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		Thread.sleep(1000);
		String dni = String.format("//table/tbody/tr[1]/td[2]");
		dni = driver.findElement(By.xpath(dni)).getText();
		assertThat(dni.trim()).isEqualToIgnoringCase(datoDni);
		String nombre = String.format("//table/tbody/tr[1]/td[1]");
		nombre = driver.findElement(By.xpath(nombre)).getText();
		assertThat(nombre.trim()).isEqualToIgnoringCase(datoNombre);
		String genero = String.format("//table/tbody/tr[1]/td[4]");
		genero = driver.findElement(By.xpath(genero)).getText();
		if(genero.trim().equals("Hombre"))
	        assertThat(datoGenero).isEqualTo("masculino");
		else if(genero.trim().equals("Mujer"))
	        assertThat(datoGenero).isEqualTo("femenino");
	}
	
}
