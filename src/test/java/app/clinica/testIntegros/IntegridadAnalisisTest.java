package app.clinica.testIntegros;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import app.GenericTest;
import app.clinica.managua.Helper;
import app.clinica.managua.PaginaAnalisis;
import app.clinica.managua.PaginaAnalisisPCR;
import app.clinica.managua.PruebaAS;
import app.clinica.managua.PruebaPCR;

public class IntegridadAnalisisTest extends GenericTest {

	private static final String PAGE_MANAGUA = "https://techsuite.gitlab.io/managua-fe/";
	private static final String PAGE_DAKAR = "https://techsuite.gitlab.io/dakar/login";
	private PaginaAnalisis PaginaAnalisis;
	private PaginaAnalisisPCR PaginaAnalisisPCR;
	private app.clinica.dakar.PaginaAnalisis PaginaAnalisisDakar;
	private app.clinica.dakar.PaginaBienvenida PaginaBienvenida;

	@Before
	public void AnalisisTestSetup() throws InterruptedException {
		driver.manage().window().maximize();
		driver.get(PAGE_MANAGUA);
		Helper.init(driver);
		app.clinica.dakar.Helper.init(driver);
		String prev = driver.getCurrentUrl();
		Helper.login("12345678N", "1111");
		Helper.fluentUrlChanged(prev);
		moveToAnalisis();
		PaginaAnalisis = new PaginaAnalisis(driver);
		PaginaAnalisisDakar = new app.clinica.dakar.PaginaAnalisis(driver);
		PaginaBienvenida = new app.clinica.dakar.PaginaBienvenida(driver);
		PaginaAnalisisPCR = new PaginaAnalisisPCR(driver);
	}

	private void moveToAnalisis() {
		String prev = driver.getCurrentUrl();
		Helper.fluentElement("//button[text()='Análisis de Sangre']").click();
		Helper.fluentUrlChanged(prev);
	}
	
	private void moveToPCR() {
		String prev = driver.getCurrentUrl();
		Helper.fluentElement("//button[text()='Pruebas PCR']").click();
		Helper.fluentUrlChanged(prev);
	}

	@Test
	public void probarConexion() throws InterruptedException {
		// Vamos a obtener todas las pruebas de sangre y pcr de un usuario. Solo una por
		// día
		// Las guardaremos en una lista de "Pruebas" con sus respectivos datos para
		// luego
		// ir a Dakar a comprobar que estén ahí

		String dniPac = "12345678A";

		List<PruebaAS> analisisManagua = getUnAnalisisPorDia(dniPac);
		
		String prev = driver.getCurrentUrl();
		Helper.fluentElement("//button[text()='Volver al inicio']").click();
		Helper.fluentUrlChanged(prev);
		moveToPCR();

		List<PruebaPCR> PCRManagua = getUnaPCRPorDia(dniPac);

		driver.get(PAGE_DAKAR);
		app.clinica.dakar.Helper.login("12345678A", "cucufuate");
		prev = driver.getCurrentUrl();
		moveToAnalisisDakar();
		app.clinica.dakar.Helper.fluentUrlChanged(prev);

		List<PruebaAS> analisisDakar = PaginaAnalisisDakar.getAS();

		List<PruebaPCR> PCRDakar = PaginaAnalisisDakar.getPCR();

		assertEquals(analisisManagua.size(), analisisDakar.size());
		assertTrue(analisisManagua.stream().allMatch(x -> analisisDakar.contains(x)));

		assertEquals(PCRManagua.size(), PCRDakar.size());
		assertTrue(PCRManagua.stream().allMatch(x -> PCRDakar.contains(x)));

	}

	private void moveToAnalisisDakar() {
		PaginaBienvenida.getButtonClick("Análisis");
	}

	private List<PruebaAS> getUnAnalisisPorDia(String dni) throws InterruptedException {
		List<PruebaAS> res = PaginaAnalisis.getPruebasFinalizadasByDNI(dni);
		List<String> fechas = res.stream().map(PruebaAS::getFecha).distinct().collect(Collectors.toList());

		List<PruebaAS> fin = new ArrayList<>();
		for (String f : fechas) {
			for (PruebaAS p : res) {
				if (p.getFecha().equals(f)) {
					fin.add(p);
					break;
				}
			}
		}
		return fin;
	}

	private List<PruebaPCR> getUnaPCRPorDia(String dni) throws InterruptedException {
		List<PruebaPCR> res = PaginaAnalisisPCR.getPCRFinalizadasByDNI(dni);
		List<String> fechas = res.stream().map(PruebaPCR::getFecha).distinct().collect(Collectors.toList());

		List<PruebaPCR> fin = new ArrayList<>();
		for (String f : fechas) {
			for (PruebaPCR p : res) {
				if (p.getFecha().equals(f)) {
					fin.add(p);
					break;
				}
			}
		}
		return fin;
	}
}
