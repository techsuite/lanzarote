package app.clinica.dakar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import app.GenericTest;

public class LangTest extends GenericTest {

	private static final String PAGE = "https://techsuite.gitlab.io/dakar/login";
	private static final String CONSULTAS = "https://techsuite.gitlab.io/dakar/base/consultas";
	private static final String ANALISIS = "https://techsuite.gitlab.io/dakar/base/analisis";
	private static final String WIKI = "https://techsuite.gitlab.io/dakar/base/wiki";
	private WholeWeb WholeWeb;

	@Before
	public void PaginaBienvenidaSetup() {
		driver.get(PAGE);
		WholeWeb = new WholeWeb(driver);
		Helper.login("00011122j", "jose1234");
	}

	/*
	 * El funcionamiento de estos tests es el siguiente: tomamos todos los strings
	 * de la pagina actual, pulsamos el boton de cambio de idioma, volvemos a tomar
	 * todos los string y comprobamos que al menos el 10% hayan cambiado para
	 * asegurarnos que se han traducido (esto lo hacemos para tener el cuenta los
	 * nombres propios que no van a cambiar).
	 */

	@Test
	public void testLangBienvenida() throws InterruptedException {
		testingChanges(0);
	}

	@Test
	public void testLangConsultas() throws InterruptedException {
		driver.get(CONSULTAS);
		Thread.sleep(1000);
		testingChanges(0);
	}

	@Test
	public void testLangAnalisis() throws InterruptedException {
		driver.get(ANALISIS);
		Thread.sleep(1000);
		testingChanges(0);
	}

	@Test
	public void testLangWiki() throws InterruptedException {
		driver.get(WIKI);
		Thread.sleep(1000);
		testingChanges(0);
	}

	private void testingChanges(double ratio) throws InterruptedException {
		List<String> actualStrings = WholeWeb.getAllStrings();
		WholeWeb.changeLang("EN");
		List<String> translatedStrings = WholeWeb.getAllStrings();
		checkIntegrity(actualStrings, translatedStrings, ratio);
	}

	/**
	 * Aquí vamos a testear que pasa si hacemos multiples cambios de idioma
	 */
	@Test
	public void multipleChanges() throws InterruptedException {
		List<String> actualStrings = WholeWeb.getAllStrings();
		WholeWeb.changeLang("ES");
		WholeWeb.changeLang("EN");
		WholeWeb.changeLang("EN");
		WholeWeb.changeLang("ES");
		List<String> newStrings = WholeWeb.getAllStrings();
		checkIntegrity(actualStrings, newStrings, 0);
	}

	private void checkIntegrity(List<String> prev, List<String> post, double ratio) {
		int diferencias = 0;
		// Comprobamos que no se haya perdido ningun String
		assertEquals(prev.size(), post.size());
		// Comprobamos que cambian al menos un 10% de los strings (para tener en cuenta
		// los nombres propios)
		for (int i = 0; i < prev.size(); i++) {
			if (!prev.get(i).equals(post.get(i)))
				diferencias++;
		}
		assertTrue((double) diferencias >= ratio * prev.size());
	}

}
