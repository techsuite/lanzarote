package app.clinica.dakar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import app.GenericTest;

public class ResgistroTest extends GenericTest {

	private static final String PAGE = "https://techsuite.gitlab.io/dakar/login";
	private static final String PAGINA_REGISTRO = "https://techsuite.gitlab.io/dakar/registro";
	private WebElement botonDNI, campoDNI, botonRegistro;
	private String dniBueno = "49817434F";
	private String emailBueno = "joseocampo@gmail.com";

	@Before
	public void PaginaBienvenidaSetup() {
		driver.get(PAGE);
		Helper.init(driver);
		botonRegistro = Helper.fluentElement("//button[@id='registro']");
		botonRegistro.click();
		Helper.fluentUrlChanged(PAGE);
		campoDNI = Helper.fluentElement("//input");
		botonDNI = Helper.fluentElement("//div[@class='card-body']/button");
	}

	@Test
	public void wrongDNI() {
		List<String> badDNIs = Arrays.asList("random", "65165165sad", "77993405Z", "71768989L", "97980472N",
				"42434286E");

		for (String dni : badDNIs) {
			campoDNI.clear();
			campoDNI.sendKeys(dni);
			botonDNI.click();
			assertEquals(PAGINA_REGISTRO, driver.getCurrentUrl());
		}
	}

	@Ignore
	@Test
	public void wrongEmail() {
		campoDNI.clear();
		campoDNI.sendKeys(dniBueno);
		String prev = driver.getCurrentUrl();
		botonDNI.click();
		Helper.fluentUrlChanged(prev);
		assertEquals("https://techsuite.gitlab.io/dakar/registro2", driver.getCurrentUrl());

		// Comprobamos que el correo coincide parcialmente
		String textoCensurado = Helper.fluentElement("//p[contains(text(),'*')]").getText();
		assertEquals(emailBueno.substring(0, 1), textoCensurado.substring(0, 1));
		assertEquals(emailBueno.substring(emailBueno.length() - 2),
				textoCensurado.substring(textoCensurado.length() - 2));

		// Introducimos un email mal, debería fallar
		Helper.fluentElement("//input").sendKeys("malemail@gmail.com");
		assertNotEquals("https://techsuite.gitlab.io/dakar/registro3", driver.getCurrentUrl());
	}


}
