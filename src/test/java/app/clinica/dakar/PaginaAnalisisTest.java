package app.clinica.dakar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import app.GenericTest;

public class PaginaAnalisisTest extends GenericTest {

	private PaginaConsultasAnalisis PaginaConsultasAnalisis;
	private PaginaBienvenida PaginaBienvenida;
	private static final String PAGE = "https://techsuite.gitlab.io/dakar/login";

	@Before
	public void PaginaBienvenidaSetup() {
		driver.get(PAGE);
		PaginaConsultasAnalisis = new PaginaConsultasAnalisis(driver);
		PaginaBienvenida = new PaginaBienvenida(driver);
		Helper.login("00011122j", "jose1234");
		moveToAnalisis();
	}

	@Test 
	public void filtroFecha() {
		List<String> res;
		// Hashmap k: filtro y v: expected
		HashMap<String, List<String>> filtro = new HashMap<>();
		filtro.put("5/4/2021", Arrays.asList("4/5/21"));
		// Comprobamos busqueda erronea
		filtro.put("1/12/2025", Arrays.asList());
		for (Entry<String, List<String>> e : filtro.entrySet()) {
			filter(e.getKey(), 1);
			res = PaginaConsultasAnalisis.getColumn("1");
			assertTrue(res.stream().allMatch(t -> e.getValue().stream().anyMatch(t::equals)));
		}
	}


	@Test
	public void filtroPrueba() {
		List<String> res;
		// Hashmap k: filtro y v: expected
		HashMap<String, List<String>> filtro = new HashMap<>();
		filtro.put("Análisis de Sangre", Arrays.asList("Análisis de Sangre"));
		// Comprobamos busqueda erronea
		filtro.put("hdhdhdhdhd", Arrays.asList());
		for (Entry<String, List<String>> e : filtro.entrySet()) {
			filter(e.getKey(), 2);
			res = PaginaConsultasAnalisis.getColumn("4");
			assertTrue(res.stream().allMatch(t -> e.getValue().stream().anyMatch(t::contains)));
		}
	}

	@Test 
	public void filtroLab() {
		List<String> res;
		// Hashmap k: filtro y v: expected
		HashMap<String, List<String>> filtro = new HashMap<>();
		filtro.put("Lab1", Arrays.asList("Lab1"));
		// Comprobamos busqueda erronea
		filtro.put("hdhdhdhdhd", Arrays.asList());
		for (Entry<String, List<String>> e : filtro.entrySet()) {
			filter(e.getKey(), 2);
			res = PaginaConsultasAnalisis.getColumn("4");
			assertTrue(res.stream().allMatch(t -> e.getValue().stream().anyMatch(t::contains)));
		}
	}

	@Test
	public void redireccionesConsultas() {
		List<WebElement> botonesTabla = PaginaConsultasAnalisis.getBotonesTabla();
		for (WebElement button : botonesTabla)
			assertEquals("/base/analisis/analisisInfo", button.getAttribute("routerlink"));
	}

	private void moveToAnalisis() {
		PaginaBienvenida.getButtonClick("Análisis");
	}

	private void filter(String filtro, int campo) {
		PaginaConsultasAnalisis.setFilterOptions(campo);
		// Caso filtro fecha
		if (campo == 1) {
			String[] arr = filtro.split("/"); // d/m/Y
			PaginaConsultasAnalisis.getFilterBox().click();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			PaginaConsultasAnalisis.selectMonth(arr[1]);
			PaginaConsultasAnalisis.selectYear(arr[2]);
			PaginaConsultasAnalisis.selectday(arr[0]);
			PaginaConsultasAnalisis.clickSearchButton();
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
			}
		} else {
			WebElement filterBox = PaginaConsultasAnalisis.getFilterBox();
			filterBox.clear();
			filterBox.sendKeys(filtro);
			// Un pequeño tiempo de espera
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}
			PaginaConsultasAnalisis.clickSearchButton();
		}
	}

}
