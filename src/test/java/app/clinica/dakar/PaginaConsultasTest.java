package app.clinica.dakar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import app.GenericTest;

public class PaginaConsultasTest extends GenericTest {

	private PaginaConsultasAnalisis PaginaConsultasAnalisis;
	private PaginaBienvenida PaginaBienvenida;
	private static final String PAGE = "https://techsuite.gitlab.io/dakar/login";

	@Before
	public void PaginaConsultasSetup() {
		driver.get(PAGE);
		PaginaConsultasAnalisis = new PaginaConsultasAnalisis(driver);
		PaginaBienvenida = new PaginaBienvenida(driver);
		Helper.login("00011122j","jose1234");
		moveToConsutas();
	}

	@Test
	public void redireccionesConsultas() {
		List<WebElement> botonesTabla = PaginaConsultasAnalisis.getBotonesTabla();
		for (WebElement button : botonesTabla)
			assertEquals("botonInfo", button.getAttribute("class"));
	}

	@Test
	public void menuButton() {
		PaginaConsultasAnalisis.openMenuButton();
		PaginaConsultasAnalisis.getMenuButtonClick("wiki", true);
		assertEquals("https://techsuite.gitlab.io/dakar/base/wiki", driver.getCurrentUrl());

		PaginaConsultasAnalisis.openMenuButton();
		PaginaConsultasAnalisis.getMenuButtonClick("consultas", true);
		assertEquals("https://techsuite.gitlab.io/dakar/base/consultas", driver.getCurrentUrl());

		PaginaConsultasAnalisis.openMenuButton();
		PaginaConsultasAnalisis.getMenuButtonClick("consultas", false);
		assertEquals("https://techsuite.gitlab.io/dakar/base/consultas", driver.getCurrentUrl());

		PaginaConsultasAnalisis.getMenuButtonClick("wiki", true);
		assertEquals("https://techsuite.gitlab.io/dakar/base/wiki", driver.getCurrentUrl());

		PaginaConsultasAnalisis.openMenuButton();
		PaginaConsultasAnalisis.getMenuButtonClick("analisis", true);
		assertEquals("https://techsuite.gitlab.io/dakar/base/analisis", driver.getCurrentUrl());

		PaginaConsultasAnalisis.openMenuButton();
		PaginaConsultasAnalisis.getMenuButtonClick("wiki", true);
		assertEquals("https://techsuite.gitlab.io/dakar/base/wiki", driver.getCurrentUrl());

		driver.navigate().back();
		assertEquals("https://techsuite.gitlab.io/dakar/base/analisis", driver.getCurrentUrl());
	}

	private void moveToConsutas() {
		PaginaBienvenida.getButtonClick("Consultas");
	}

	/*
	 * 0 - Todo, 1 - Fecha, 2 - DNI Doctor
	 */
	private void filter(String filtro, int campo) {
		PaginaConsultasAnalisis.setFilterOptions(campo);
		// Caso filtro fecha
		if (campo == 1) {
			String[] arr = filtro.split("/"); // d/m/Y
			PaginaConsultasAnalisis.getFilterBox().click();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			PaginaConsultasAnalisis.selectMonth(arr[1]);
			PaginaConsultasAnalisis.selectYear(arr[2]);
			PaginaConsultasAnalisis.selectday(arr[0]);
			PaginaConsultasAnalisis.clickSearchButton();
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
			}
		} else {
			WebElement filterBox = PaginaConsultasAnalisis.getFilterBox();
			filterBox.clear();
			filterBox.sendKeys(filtro);
			// Un pequeño tiempo de espera
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}
			PaginaConsultasAnalisis.clickSearchButton();
		}
	}
}
