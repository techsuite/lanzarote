package app.clinica.dakar;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import app.GenericTest;

public class PaginaBienvenidaTest extends GenericTest {

	private PaginaBienvenida PaginaBienvenida;
	private static final String PAGE = "https://techsuite.gitlab.io/dakar/login";

	@Before
	public void PaginaBienvenidaSetup() {
		driver.get(PAGE);
		PaginaBienvenida = new PaginaBienvenida(driver);
		Helper.login("00011122j","jose1234");
	}

	// Testear redirecciones de los botones
	@Test
	public void redireccionesBotones() throws InterruptedException {
		// Comprobamos los botones
		Thread.sleep(500);
		PaginaBienvenida.getButtonClick("Consultas");
		assertEquals("https://techsuite.gitlab.io/dakar/base/consultas", driver.getCurrentUrl());
		driver.navigate().back();
		
		Thread.sleep(500);
		PaginaBienvenida.getButtonClick("Análisis");
		assertEquals("https://techsuite.gitlab.io/dakar/base/analisis", driver.getCurrentUrl());
		driver.navigate().back();
		
		Thread.sleep(500);
		PaginaBienvenida.getButtonClick("Wiki");
		assertEquals("https://techsuite.gitlab.io/dakar/base/wiki", driver.getCurrentUrl());

	}

	// Testear boton de salir
	@Test
	public void botonSalir() {
		PaginaBienvenida.clickSignOutButton();
		assertEquals("https://techsuite.gitlab.io/dakar/", driver.getCurrentUrl());
	}

}
