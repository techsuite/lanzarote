package app.clinica.dakar;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import app.GenericTest;

public class AccederSinCredencialesTest extends GenericTest {

	// URL's que necesitarian credenciales previas
	private static final String pag_bienvenida_usuario = "https://techsuite.gitlab.io/dakar/base/pagina-bienvenida";
	private static final String pag_consulta = "https://techsuite.gitlab.io/dakar/base/consultas";
	private static final String pag_analisis = "https://techsuite.gitlab.io/dakar/base/analisis";
	private static final String pag_Techsuite = "https://techsuite.gitlab.io/dakar/base/wiki";
	private static final String pag_busqueda_medicamento = "https://techsuite.gitlab.io/dakar/base/wiki/medicamento";
	private static final String[] pags = { pag_bienvenida_usuario, pag_consulta, pag_analisis, pag_Techsuite,
			pag_busqueda_medicamento };

	// URL del login
	private static final String pag_login = "https://techsuite.gitlab.io/dakar/login";

	@Test
	public void intentaAcceder() {
		for (int i = 0; i < pags.length; i++) {
			driver.get(pags[i]);
			assertThat(driver.getCurrentUrl()).isEqualTo(pag_login);
		}
	}
}
