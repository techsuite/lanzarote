package app.clinica.dakar;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import app.GenericTest;

public class BienvenidaPacientesTest extends GenericTest {

	private BienvenidaPacientes BienvenidaPacientes;
	private static final String HOME_PAGE_URL = "https://techsuite.gitlab.io/dakar";

	@Before
	public void BienvenidaClientesSetup() {
		driver.get(HOME_PAGE_URL);
		BienvenidaPacientes = new BienvenidaPacientes(driver);
	}

	@Test
	public void checkLiterals() {

		String titulo = BienvenidaPacientes.getTitle();
		String texto = BienvenidaPacientes.getText();
		// boton

		// Titulo
		assertNotNull(titulo);
		assertThat(titulo).isEqualTo("Bienvenido a la Clínica April");
		// Subtítulo
		assertNotNull(texto);
		assertThat(texto).isEqualTo(
				"La Clínica April destaca por sus innovadores cuidados y por sus cualificados profesionales, así como por su cercanía con los pacientes. Introduzca sus credenciales, pulsando en “Iniciar sesión”, para consultar su historial médico.");

	}

	@Ignore
	@Test
	public void checkTechsuiteLogo() {
		assertNotNull(BienvenidaPacientes
				.getTechsuiteLogo("https://drive.google.com/uc?id=11S5p_sDmewpkVvXgnvdmj8SmkhX1RIJf"));
	}

	@Test
	public void checkIniciarSesionButton() {
		String currentUrl = HOME_PAGE_URL;

		BienvenidaPacientes.clickOn("Iniciar Sesión");
		assertThat(driver.getCurrentUrl()).isEqualTo(currentUrl + "/login");

	}

	@Test
	public void checkNavBar() {

		List<String> literals = BienvenidaPacientes.getLiterals();

		assertThat(literals.get(0)).isEqualTo("Inicio");
		assertThat(literals.get(1)).isEqualTo("Login");
	}

	@Test
	public void checkNavigation() {

		String currentUrl = HOME_PAGE_URL;

		BienvenidaPacientes.clickOnNavBar("Inicio");
		assertThat(driver.getCurrentUrl()).isEqualTo(currentUrl + "/");

		BienvenidaPacientes.clickOnNavBar("Login");
		assertThat(driver.getCurrentUrl()).isEqualTo(currentUrl + "/login");
	}

}
