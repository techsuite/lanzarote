package app.clinica.dakar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import app.GenericTest;

public class WikiTest extends GenericTest {

	private Wiki Wiki;
	private PaginaBienvenida PaginaBienvenida;
	private static final String PAGE = "https://techsuite.gitlab.io/dakar/login";

	@Before
	public void WikiSetup() {
		driver.get(PAGE);
		Wiki = new Wiki(driver);
		PaginaBienvenida = new PaginaBienvenida(driver);
		Helper.login("00011122j", "jose1234");
		moveToWiki();
	}

	@Test
	public void addingAndRemovingFields() {
		Wiki.getAddButton1().click();
		try {
			Thread.sleep(500);
		} catch (Exception e) {
		}
		Wiki.getAddButton2().click();
		Wiki.getAddButton1().click();
		Wiki.getAddButton2().click();

		assertEquals(Wiki.getAddButtons().size(), 2);
		assertEquals(Wiki.getRemoveButtons().size(), 2);
		assertEquals(Wiki.getInputMultiple().size(), 2);
		assertFalse(Wiki.sendButtonVisible());

		Wiki.getRemoveButton1().click();
		Wiki.getRemoveButton1().click();

		assertEquals(Wiki.getAddButtons().size(), 1);
		assertEquals(Wiki.getRemoveButtons().size(), 1);
		assertEquals(Wiki.getInputMultiple().size(), 0);
		assertTrue(Wiki.sendButtonVisible());
	}

	@Test
	public void oneDrug() {
		String prevUrl;
		List<String> names = new ArrayList<>();
		names.add("Penicilina");
		names.add("Paracetamol");

		for (String name : names) {
			Wiki.getInput1().sendKeys(name);

			prevUrl = driver.getCurrentUrl();
			Wiki.getSendButton().click();

			Helper.fluentUrlChanged(prevUrl);

			assertEquals(driver.getCurrentUrl(), "https://techsuite.gitlab.io/dakar/base/wiki/medicamento");

			// Creamos el objeto PaginaMedicamento
			PaginaMedicamento PaginaMedicamento = new PaginaMedicamento(driver);

			// Check header
			assertEquals(PaginaMedicamento.getHeader(), name);

			// Coprobar campos y existencia de texto
			assertEquals(PaginaMedicamento.getFields(),
					Arrays.asList(new String[] { "Contraindicaciones", "Posología", "Prospecto", "Composición" }));
			assertEquals(PaginaMedicamento.getTexts().size(), 4);

			prevUrl = driver.getCurrentUrl();

			// Comprobar enlace
			WebElement moreInfoButton = PaginaMedicamento.getMoreInfoButton();
			Helper.clickHiddenButton(moreInfoButton);
			Helper.fluentUrlChanged(prevUrl);
			// assertTrue(driver.getCurrentUrl().contains(name));

			prevUrl = driver.getCurrentUrl();
			driver.navigate().back();
			Helper.fluentUrlChanged(prevUrl);
			// Con uno valdría pero para saltar el bug
			prevUrl = driver.getCurrentUrl();
			driver.navigate().back();
			Helper.fluentUrlChanged(prevUrl);
		}
	}

	@Test
	public void twoDrugs() {

		String prevUrl;

		Wiki.getAddButton1().click();

		Wiki.getInput1().sendKeys("Penicilina");
		Wiki.getInput2().sendKeys("Paracetamol");

		prevUrl = driver.getCurrentUrl();
		Wiki.getCompareButton().click();

		Helper.fluentUrlChanged(prevUrl);

		assertEquals(driver.getCurrentUrl(), "https://techsuite.gitlab.io/dakar/base/wiki/comparacion");

		// Creamos el objeto PaginaMedicamento
		PaginaComparacion PaginaComparacion = new PaginaComparacion(driver);

		// Check headers match
		assertEquals(PaginaComparacion.getHeaders(), Arrays.asList(new String[] { "Penicilina", "Paracetamol" }));
		assertEquals(PaginaComparacion.getTexts().size(), 2);
		assertEquals(PaginaComparacion.getFields(),
				Arrays.asList(new String[] { "Contraindicaciones", "Contraindicaciones" }));

		// Check Botones
		WebElement button1 = PaginaComparacion.getMoreInfoButtons().get(0);
		WebElement button2 = PaginaComparacion.getMoreInfoButtons().get(1);

		prevUrl = driver.getCurrentUrl();

		Helper.clickHiddenButton(button1);
		Helper.fluentUrlChanged(prevUrl);

		// Aqui podriamos hacer mas tests como volver para probar mas cosas pero por el
		// bug de los reloads no se puede aun

		prevUrl = driver.getCurrentUrl();
		driver.navigate().back();
		Helper.fluentUrlChanged(prevUrl);
		// Con uno valdría pero para saltar el bug
		prevUrl = driver.getCurrentUrl();
		driver.navigate().back();
		Helper.fluentUrlChanged(prevUrl);
	}

	private void moveToWiki() {
		PaginaBienvenida.getButtonClick("Wiki");
	}
}
