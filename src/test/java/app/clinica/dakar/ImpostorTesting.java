package app.clinica.dakar;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import app.GenericTest;

public class ImpostorTesting extends GenericTest {

	private static final String PAGE = "https://techsuite.gitlab.io/dakar/login";
	private static final String PAGE_IMPOSTOR = "https://techsuite.gitlab.io/dakar/base/impostor";
	private String token = "12345";
	private String tokenOwner = "Pepe";

	@Before
	public void PaginaBienvenidaSetup() {
		Helper.init(driver);
		driver.get(PAGE);
		Helper.login("89328314X", "contra123");
	}

	@Test
	public void redireccion() {
		String url = driver.getCurrentUrl();
		assertEquals(PAGE_IMPOSTOR, url);
	}

	@Test
	public void accesoCorrecto() {
		Helper.fluentElement("//input").sendKeys(token);
		Helper.fluentElement("//div[@class='card-body']/button").click();
		Helper.fluentUrlChanged(PAGE_IMPOSTOR);
		assertEquals("https://techsuite.gitlab.io/dakar/base/pagina-bienvenida", driver.getCurrentUrl());
		assertEquals(tokenOwner, Helper.fluentElement("//h1[text()='Pepe']").getText());
	}

	@Test
	public void accesoIncorrecto() {
		String[] badTokens = new String[] { "adaf", "q4534", "14235", "43895943", "q87q48756q8374568q73465874356" };

		WebElement textField = Helper.fluentElement("//input");
		WebElement button = Helper.fluentElement("//div[@class='card-body']/button");

		for (String s : badTokens) {
			textField.clear();
			textField.sendKeys(s);
			button.click();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}
			assertEquals(PAGE_IMPOSTOR, driver.getCurrentUrl());
		}
	}

}
