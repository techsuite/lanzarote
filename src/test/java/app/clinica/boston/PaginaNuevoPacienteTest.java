package app.clinica.boston;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.GenericTest;

public class PaginaNuevoPacienteTest extends GenericTest{

	private PaginaNuevoPaciente PagNewPaciente;
	
	private static final String LOGIN_URL = "https://techsuite.gitlab.io/boston-fe/login";
	private static final String NEW_PACIENTE_URL = "https://techsuite.gitlab.io/boston-fe/nuevo_paciente";
	private static final String NEW_CONSULTA_URL = "https://techsuite.gitlab.io/boston-fe/nueva_consulta";
	private static final String FORMULARIO_PACIENTE_URL = "https://techsuite.gitlab.io/boston-fe/nuevo_paciente/formulario";
	
	private static final String DNI_FILTER = "84739487D";
	
	private static final String DNI = "11111111k";
	private static final String PASS = "1234";
	
	@Before
	public void PaginaNuevoPacienteTestSetUp() throws InterruptedException {
		driver.get(LOGIN_URL);
		PagNewPaciente = new PaginaNuevoPaciente(driver);
		Helper.init(driver);
		Helper.login(DNI, PASS);
		driver.get(NEW_PACIENTE_URL);
		Thread.sleep(500);
	}
	
	/*
	 * Este test hace las siguientes aserciones:
	 * 	1- Los textos de los botones son correctos
	 * 	2- Los textos del titulo, filtro y botones son correctos
	 * 	3- Comprueba que los textos de la tabla son correctos
	 * 	4- La pagina actual es 1 y el boton Anterior todavia no es visible
	 * 	5- Al recorrer las siguientes paginas de tablas con el boton Siguiente
	 * los textos son correctos, el boton Anterior es visible y el boton Siguiente
	 * tiene que desaparecer cuando se alcance el maximo de paginas
	 * 	6- Se recorre las anteriores paginas con el boton Anterior hasta que este desaparezca
	 */
	@Test
	public void checkTitlesAndLiterals() {
		
		assertThat(PagNewPaciente.getTopButtonNuevaConsulta().getText()).isEqualTo("Nueva Consulta");
		assertThat(PagNewPaciente.getTopButtonNuevoPaciente().getText()).isEqualTo("Nuevo Paciente");
		
		assertThat(PagNewPaciente.getButtonLogout().getText()).isEqualTo("Cerrar sesión");
		
		assertThat(PagNewPaciente.getTitle()).contains("Pacientes de otros doctores");
		
		assertThat(PagNewPaciente.getFilterInput().getAttribute("placeholder")).isEqualTo("Buscar por dni");
		assertThat(PagNewPaciente.getButtonBuscar().getText()).isEqualTo("Buscar");
		
		assertThat(PagNewPaciente.getButtonPacienteNuevoIngreso().getText()).isEqualTo("Paciente nuevo ingreso");
		
		checkTable();
		
		assertTrue(isNotOutOfBounds());
		
		boolean correcto = false;
		try {
			PagNewPaciente.getUnsafeLastPageButton();
		} catch(Exception e) {
			correcto = true;
		}
		assertTrue(correcto);
		
		int pagActual = getActualPage();
		assertTrue(pagActual == 1);
		
		int pagsTotales = getTotalPages();
		correcto = false;
		try {
			int i = 1;
			WebElement botonSiguiente;
			WebElement botonAnterior;
			while(i < pagsTotales+2) {
				botonSiguiente = PagNewPaciente.getUnsafeNextPageButton();
				assertThat(botonSiguiente.getText()).isEqualTo("Siguiente");
				botonSiguiente.click();
				Thread.sleep(400);
				botonAnterior = PagNewPaciente.getLastPageButton();
				assertThat(botonAnterior == null ? "" : botonAnterior.getText()).isEqualTo("Anterior");
				i++;
				assertTrue(isNotOutOfBounds());
				assertTrue(i==getActualPage());
				checkTable();
			}
		}catch(Exception e) {
			correcto = true;
		}
		assertTrue(correcto);
		
		assertTrue(pagsTotales == getActualPage());
		
		correcto = false;
		try {
			int i = pagsTotales;
			WebElement botonAnterior;
			while(i>-2) {
				botonAnterior = PagNewPaciente.getLastPageButton();
				assertThat(botonAnterior.getText()).isEqualTo("Anterior");
				botonAnterior.click();
				Thread.sleep(400);
				i--;
				assertTrue(isNotOutOfBounds());
				assertTrue(i==getActualPage());
			}
		}catch(Exception e) {
			correcto = true;
		}
		assertTrue(correcto);
		
		assertTrue(1 == getActualPage());
		
	}
	
	/*
	 * Este test hace las siguiente aserciones en el filtro:
	 * 	1- Al introducir el primer numero de un DNI salen todos los DNIs que empiezan por este
	 * 	2- Al introducir el DNI salen todos los mismos DNIs
	 */
	@Test
	public void checkFilter() throws InterruptedException {
		
		String key = DNI_FILTER.substring(0, 1);
		
		PagNewPaciente.getFilterInput().sendKeys(key);
		PagNewPaciente.getButtonBuscar().click();
		Thread.sleep(400);
		
		WebElement botonAvanzar = PagNewPaciente.getNextPageButton();
		checkTableByDNI(key);
		while(botonAvanzar != null) {
			botonAvanzar.click();
			Thread.sleep(400);
			botonAvanzar = PagNewPaciente.getNextPageButton();
			checkTableByDNI(key);
		}
		
		key = DNI_FILTER.substring(1);
		
		PagNewPaciente.getFilterInput().sendKeys(key);
		PagNewPaciente.getButtonBuscar().click();
		Thread.sleep(400);
		
		botonAvanzar = PagNewPaciente.getNextPageButton();
		checkTableByDNI(DNI_FILTER);
		while(botonAvanzar != null) {
			botonAvanzar.click();
			Thread.sleep(400);
			botonAvanzar = PagNewPaciente.getNextPageButton();
			checkTableByDNI(DNI_FILTER);
		}
		
	}
	
	/*
	 * Este test hace las siguientes aserciones:
	 * 	1- Clickear en el boton asociar paciente y cancelar operacion
	 * 	2- Clickear en el boton superior nuevo paciente no cambia la pagina
	 * 	3- Clickear en el boton superior nueva consulta no cambia la pagina
	 * 	4- Clickear en el boton cerrar sesion y desconectarse
	 */
	@Test
	public void checkNavButton() throws InterruptedException {
		
		PagNewPaciente.getButtonAsociar(0).click();
		PagNewPaciente.cancelAction();
		
		PagNewPaciente.getTopButtonNuevoPaciente().click();
		Thread.sleep(500);
		assertThat(driver.getCurrentUrl()).isEqualTo(NEW_PACIENTE_URL);
			
		PagNewPaciente.getTopButtonNuevaConsulta().click();
		new WebDriverWait(driver, Duration.ofSeconds(2))
			.until(driver -> driver.getCurrentUrl().equals(NEW_CONSULTA_URL));
			
		driver.get(NEW_PACIENTE_URL);
		Thread.sleep(500);
		
		PagNewPaciente.getButtonPacienteNuevoIngreso().click();
		new WebDriverWait(driver, Duration.ofSeconds(2))
			.until(driver -> driver.getCurrentUrl().equals(FORMULARIO_PACIENTE_URL));
			
		driver.get(NEW_PACIENTE_URL);
		Thread.sleep(500);
			
		PagNewPaciente.getButtonLogout().click();
		PagNewPaciente.acceptAction();
		new WebDriverWait(driver, Duration.ofSeconds(2))
			.until(driver -> driver.getCurrentUrl().equals(LOGIN_URL));
	}
	
	/*
	 * Funcion auxiliar que recoge las entradas de una tabla y comprueba que los DNIs comienzan por el String proporcionado
	 */
	private void checkTableByDNI(String dni) {
		
		int tamanoTabla = PagNewPaciente.getTableLength();
		for(int i = 0; i < tamanoTabla; i++)
			assertThat(PagNewPaciente.getTableEntry(i).get(0)).startsWith(dni);
		
	}
	
	/*
	 * Funcion auxiliar que recoge todos los datos de la tabla (cabecera y entradas)
	 * A su vez, hace las siguientes aserciones:
	 * 	1- La cabecera tiene los textos correctos
	 * 	2- No existen entradas con datos vacios en el dni, nombre, etc.
	 */
	private void checkTable() {
		
		List<String> nombresCabecera = PagNewPaciente.getTableHeader();
		int tamanoTabla = PagNewPaciente.getTableLength();
		List<List<String>> entradas = new ArrayList<>();
		for(int i = 0; i < tamanoTabla; i++) {
			entradas.add(PagNewPaciente.getTableEntry(i));
			assertThat(PagNewPaciente.getButtonAsociar(i).getText()).isEqualTo("Asociar este paciente con usted");
		}
		
		assertThat(nombresCabecera.get(0)).isEqualTo("DNI");
		assertThat(nombresCabecera.get(1)).isEqualTo("Nombre");
		assertThat(nombresCabecera.get(2)).isEqualTo("Apellidos");
		assertThat(nombresCabecera.get(3)).isEqualTo("Género");
		assertThat(nombresCabecera.get(4)).isEqualTo("Fecha del nacimiento");
		assertThat(nombresCabecera.get(5)).isEqualTo("Correo Electrónico");
		assertThat(nombresCabecera.get(6)).isEqualTo("Fecha del registro");
		assertThat(nombresCabecera.get(7)).isEqualTo("Fecha de la última consulta");
		for(List<String> listaDeDatos : entradas) {
			for(int i = 0; i < 7; i++)
				assertThat(listaDeDatos.get(i)).isNotEmpty();
			//assertTrue(isDNI(listaDeDatos.get(0)));
		}
		
	}
	
	/*
	 * Función auxiliar que convierte Strings numericos a int
	 */
	private int atoi(String palabra) {
		int longitud = palabra.length();
		int num = 0;
		for(int i = longitud-1; i >= 0; i--)
			num += (palabra.charAt(i)-'0')*(Math.pow(10,longitud-i-1));
		return num;
	}
	
	/*
	 * Funcion auxiliar que devuelve la pagina actual
	 */
	private int getActualPage() {
		return atoi(PagNewPaciente.getPage().split("/")[0]);
	}
	
	/*
	 * Funcion auxiliar que devuelve el total de paginas
	 */
	private int getTotalPages() {
		return atoi(PagNewPaciente.getPage().split("/")[1]);
	}
	
	/*
	 * Funcion auxiliar booleana que comprueba si el numero actual de pagina supera el limite o es inferior a 1
	 */
	private boolean isNotOutOfBounds() {
		int actual = getActualPage();
		return PagNewPaciente.getPage().contains("/") && actual > 0  && actual <= getTotalPages();
	}
	
	/* Funcion a utilizar en el futuro (?)
	private boolean isDNI(String dni) {
		boolean correcto = dni != null && dni.length() == 8;
		if(correcto) {
			
			for(int i = 0; i < 7; i++) {
				char digito = dni.charAt(i);
				correcto = '0' <= digito && digito <= '9';
			}
			char letra = dni.charAt(7);
			correcto = correcto && 'A' <= letra && letra <= 'Z' && 'a' <= letra && letra <= 'z';
		}
		return correcto;
	}
	*/
	
}
