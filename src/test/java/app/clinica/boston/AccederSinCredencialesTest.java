package app.clinica.boston;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import app.GenericTest;

public class AccederSinCredencialesTest extends GenericTest{

	private static final String login_url = "https://techsuite.gitlab.io/boston-fe/login";
	private static final String home_url = "https://techsuite.gitlab.io/boston-fe/home";
	private static final String nuevacons_url = "https://techsuite.gitlab.io/boston-fe/nueva_consulta";
	private static final String nuevopac_url = "https://techsuite.gitlab.io/boston-fe/nuevo_paciente";
	private static final String registrarpac_url = "https://techsuite.gitlab.io/boston-fe/nuevo_paciente/formulario";

	static final String[] pags = {home_url, nuevacons_url, nuevopac_url, registrarpac_url};
	
	@Test
	public void intentaAcceder() {
		for (int i = 0; i < pags.length; i++) {
			driver.get(pags[i]);
			assertThat(driver.getCurrentUrl()).isEqualTo(login_url);
		}
	}
}
