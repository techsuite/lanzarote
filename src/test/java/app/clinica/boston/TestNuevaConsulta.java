package app.clinica.boston;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;

import app.GenericTest;

public class TestNuevaConsulta extends GenericTest {

	private PaginaNuevaConsulta PaginaNuevaConsulta;
	private static final String PAGE = "https://techsuite.gitlab.io/boston-fe/login";

	@Before
	public void PaginaNuevaConsultaSetup() {
		driver.get(PAGE);
		PaginaNuevaConsulta = new PaginaNuevaConsulta(driver);
		Helper.init(driver);
		Helper.login("00000000v", "asdf");
	}

	// Tests para Nueva Consulta

	@Test
	public void testDesconexion() {
		assertEquals("https://techsuite.gitlab.io/boston-fe/login", PaginaNuevaConsulta.disconnect());
	}

	@Test
	public void testBarraNavegacion() {
		// Implicitamente comprobamos la existencia
		Helper.fluentElement("//a[text()='Nueva Consulta']");
		// Comprobamos el otro boton
		String prev = driver.getCurrentUrl();
		Helper.fluentElement("//a[text()='Nuevo Paciente']").click();
		Helper.fluentUrlChanged(prev);
		assertEquals("https://techsuite.gitlab.io/boston-fe/nuevo_paciente", driver.getCurrentUrl());
	}

	@Test
	public void testBuscador() throws InterruptedException {
		List<String> expected = Arrays.asList("000", "04357896S", "05");
		for (String s : expected) {
			PaginaNuevaConsulta.filtrar(s);
			Thread.sleep(500);
			List<String> dnisPacientes = PaginaNuevaConsulta.getPacientes();
			assertTrue(dnisPacientes.size() > 0);
			assertTrue(dnisPacientes.stream().allMatch(x -> x.contains(s)));
		}
	}

	@Test
	public void testPaginacion() throws InterruptedException {
		// Cogemos los pacientes mostrados actualmente
		List<String> pacientes1 = PaginaNuevaConsulta.getPacientes();

		// Avanzamos de pagina y volvemos a coger los pacientes, esperamos que sean
		// distintos
		PaginaNuevaConsulta.avanzarPagina();
		List<String> pacientes2 = PaginaNuevaConsulta.getPacientes();
		assertNotEquals(pacientes1, pacientes2);
		assertTrue(pacientes1.size() >= pacientes2.size());

		// Volvemos y vemos si ahora siguen coincidiendo
		PaginaNuevaConsulta.retrocederPagina();
		assertEquals(pacientes1, PaginaNuevaConsulta.getPacientes());

		// Pulsamos muchas veces ambos botones y nada deberia romperse
		PaginaNuevaConsulta.avanzarPagina();
		PaginaNuevaConsulta.retrocederPagina();
		PaginaNuevaConsulta.avanzarPagina();
		PaginaNuevaConsulta.retrocederPagina();
		PaginaNuevaConsulta.avanzarPagina();
		PaginaNuevaConsulta.retrocederPagina();
	}

	@Test
	public void testConsultasPaciente() throws InterruptedException {
		List<String> pacientes = PaginaNuevaConsulta.getPacientes();
		for (String paciente : pacientes) {
			PaginaNuevaConsulta.getBotonConsultas(paciente).click();
			Thread.sleep(200);
			((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
			assertTrue(Helper.fluentElement("//h2[text()=' NO TIENE CONSULTAS PREVIAS. ']").isDisplayed()
					^ Helper.fluentElement("//table[@id='consPrevias']").isDisplayed());
		}
	}


	@Test
	public void testBotonNuevaConsulta() throws InterruptedException {
		List<String> pacientes = PaginaNuevaConsulta.getPacientes();
		for (String paciente : pacientes) {
			PaginaNuevaConsulta.getBotonConsultas(paciente).click();
			Thread.sleep(200);
			((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
			// En caso de que no se encuentre el elemento capturaremos el fallo como una
			// excepcion del fluentWait
			Helper.fluentElement("//button[@id='anadir-consulta']");
		}
	}

	@Test
	public void testAdding() throws InterruptedException {
		List<String> dnisPacientes = PaginaNuevaConsulta.getPacientes();
		int N_INTENTOS = 10;
		for (int i = 0; i < N_INTENTOS; i++) {
			// Cogemos un paciente aleatorio
			int random = ThreadLocalRandom.current().nextInt(0, dnisPacientes.size());

			String dniPaciente = dnisPacientes.get(random);
			String enfermedades = "faringitis.gripe";
			String medicamentos = "ibuprofeno.frenadol";
			String pruebas = "analisis de sangre.pcr";
			String observaciones = "Nada que añadir";

			// Añadimos la consulta
			PaginaNuevaConsulta.addConsulta(dniPaciente, enfermedades, medicamentos, pruebas, observaciones);
			Thread.sleep(500);

			PaginaNuevaConsulta.getBotonConsultas(dniPaciente).click();
			Thread.sleep(500);
			List<Consulta> consultas = PaginaNuevaConsulta.getConsultas(dniPaciente);

			// Buscamos el id de la consulta que acabamos de introducir
			String id = getID(consultas, enfermedades, medicamentos, pruebas, observaciones);

			// Comprobamos que se ha introducido correctamente
			//assertNotNull(id);
		}
	}

	// Métodos auxiliares

	/*
	 * Método para obtener el id relacionado con una consulta de la tabla
	 * correspondiente
	 */
	private String getID(List<Consulta> consultas, String enfermedades, String medicamentos, String pruebas,
			String observaciones) {
		for (Consulta c : consultas) {
			if (c.getEnfermedades().equals(enfermedades) && c.getMedicamentos().equals(medicamentos)
					&& c.getPruebas().equals(pruebas) && c.getObservaciones().equals(observaciones))
				return c.getId();
		}
		return null;
	}
}
