package app.clinica.boston;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import app.GenericTest;

public class LoginTest extends GenericTest{

	private Login Login;
	private static final String RAIZ_URL = "https://techsuite.gitlab.io/boston-fe/";
	private static final String LOGIN_URL = RAIZ_URL + "login";
	private String user = "00000000v";
	private String pssw = "asdf";
	private String pssw2 = "1234";

	public void checkErrorMsg1() {
		String msg = Login.getErrorMsg1();
		assertNotNull(msg);
		assertThat(msg).contains("incorrectos");
	}
	
	public void checkErrorMsg2() {
		String msg = Login.getErrorMsg2();
		assertNotNull(msg);
		assertThat(msg).contains("DNI");
	}
	
	@Before
	public void LoginSetup() {
		driver.get(LOGIN_URL);
		Login = new Login(driver);
	}
	
	@Test
	public void checkLoginAndHomePage() throws InterruptedException {
		Login.login(user,pssw);
		Thread.sleep(1500);
		assertThat(driver.getCurrentUrl()).isEqualTo(RAIZ_URL + "home");
		Thread.sleep(5000);
		assertThat(driver.getCurrentUrl()).isEqualTo(RAIZ_URL + "nueva_consulta");
	}
	@Test
	public void checkLiterals() {

		String titulo = Login.getTitle();

		assertNotNull(titulo);
		assertThat(titulo).isEqualTo("Login");
	}
	@Test
	public void checkHomeLiterals() throws InterruptedException {
		Login.login(user,pssw);
		Thread.sleep(1500);
		String titulo = Login.getTitleHome();
		String texto = Login.getTextHome();
		assertNotNull(titulo);
		assertThat(titulo).isEqualTo("Bienvenido");
		assertNotNull(texto);
		assertThat(texto).contains("espere");
	}
	@Test
	public void checkHomeBtn() throws InterruptedException {
		Login.login(user,pssw);
		Thread.sleep(1500);
		Login.buttonHome();
		Thread.sleep(1500);
		assertThat(driver.getCurrentUrl()).isEqualTo(RAIZ_URL + "nueva_consulta");
	}
	
	@Test
	// password vacio user correcto
	public void checkLogin1() throws InterruptedException {

		Login.login(user, "");
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg1();
	}
	
	@Test
	// user vacio password correcto
	public void checkLogin2() throws InterruptedException {

		Login.login("", pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg2();
	}

	@Test
	// user espacios password correcto
	public void checkLogin3() throws InterruptedException {

		Login.login("      ", pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg2();
	}

	@Test
	// user correcto password espacios
	public void checkLogin4() throws InterruptedException {

		Login.login(user, "      ");
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg1();
	}

	@Test
	// user String aleatorio password correcto
	public void checkLogin5() throws InterruptedException {

		Login.login("miguel", pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg2();
	}

	@Test
	// user correcto password String aleatorio
	public void checkLogin6() throws InterruptedException {

		Login.login(user, "miguel");
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg1();
	}

	@Test
	// user seguido de espacios password correcto
	public void checkLogin7() throws InterruptedException {

		Login.login(user + "  ", pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg2();
	}
	
	@Test
	// user correcto password seguido de espacios
	public void checkLogin8() throws InterruptedException {

		Login.login(user, pssw + "  ");
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg1();
	}

	@Test
	// user duplicado password correcto
	public void checkLogin9() throws InterruptedException {

		Login.login(user + user, pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg2();
	}

	@Test
	// user correcto password duplicado
	public void checkLogin10() throws InterruptedException {

		Login.login(user, pssw + pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg1();
	}

	@Test
	// user DNI false password correcto
	public void checkLogin11() throws InterruptedException {

		Login.login("07939829B", pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg1();
	}

	@Test
	// sobrepasa numero de caracteres en user
	public void checkLogin12() throws InterruptedException {
		String aux = "";
		for (int i = 0; i < 100; i++) {
			aux += user;
		}
		Login.login(aux, pssw);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg2();
	}

	@Test
	// sobrepasa numero de caracteres en password
	public void checkLogin13() throws InterruptedException {
		String aux = "";
		for (int i = 0; i < 100; i++) {
			aux += pssw;
		}
		Login.login(user, aux);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg1();
	}

	@Test
	// user correcto password en mayusculas
	public void checkLogin14() throws InterruptedException {
		Login.login(user, pssw.toUpperCase());

		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg1();
	}
	
	@Test
	//user correcto y password de otro user correcto
	public void checkLogin15() throws InterruptedException {
		Login.login(user, pssw2);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg1();
	}
	
	@Test
	//user correcto y password de otro user correcto
	public void checkLogin16() throws InterruptedException {
		Login.login(user, pssw2);
		Thread.sleep(2000);
		assertThat(driver.getCurrentUrl()).isEqualTo(LOGIN_URL);
		checkErrorMsg1();
	}
	
}
