package app.clinica.boston;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.time.Duration;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.GenericTest;

public class FormularioNuevoPacienteTest extends GenericTest{

	private FormularioNuevoPaciente formularioNuevoPaciente;
	private static final String LOGIN_URL = "https://techsuite.gitlab.io/boston-fe/login";
	private static final String FORMULARIO_URL = "https://techsuite.gitlab.io/boston-fe/nuevo_paciente/formulario";
	private static final String RETURN_PAGE_URL = "https://techsuite.gitlab.io/boston-fe/nuevo_paciente";
	
	private static final String DNI = "00000000v";
	private static final String PASS = "asdf";
	
	@Before
	public void formularioNuevoPacienteSetUp() {
		driver.get(LOGIN_URL);
		formularioNuevoPaciente = new FormularioNuevoPaciente(driver);
		Helper.init(driver);
		Helper.login(DNI, PASS);
		driver.get(FORMULARIO_URL);
	}
	
	/* Este test realiza las aserciones en el siguiente orden:
	 * 	1- El titulo y subtitulo son correctos
	 * 	2- El nombre de los inputs son correctos
	 * 	3- Si no se ha intentado escribir, no debe aparecer el "Campo obligatorio"
	 * 	4- Si se ha intentado escribir, debe aparecer el "Campo obligatorio"
	 * 	5- El boton de Enviar esta descativado pero se activa al escribir en los campos
	 */
	@Test
	public void checkLiterals() throws InterruptedException {
		
		assertThat(formularioNuevoPaciente.getTitle()).isEqualTo("Registrar paciente");
		assertThat(formularioNuevoPaciente.getSubtitle()).isEqualTo("Introduzca los datos del nuevo paciente a registrar en el sistema.");
		
		assertThat(formularioNuevoPaciente.getInputLabelText(0)).isEqualTo("DNI");
		assertThat(formularioNuevoPaciente.getInputLabelText(1)).isEqualTo("Nombre");
		assertThat(formularioNuevoPaciente.getInputLabelText(2)).isEqualTo("Apellidos");
		assertThat(formularioNuevoPaciente.getInputLabelText(3)).isEqualTo("Género");
		assertThat(formularioNuevoPaciente.getInputLabelText(4)).isEqualTo("Fecha de nacimiento");
		assertThat(formularioNuevoPaciente.getInputLabelText(5)).isEqualTo("Email");

		boolean noErrorMessage = true;
		for(int i = 0; i < 5; i++) {
			try {
				formularioNuevoPaciente.getInputErrorText(i);
				noErrorMessage = false;
			}catch(Exception e) {}
		}
		assertTrue(noErrorMessage);
		
		System.err.print("ERROR");
		
		for(int i = 0; i < 3; i++) {
			formularioNuevoPaciente.getInput(i).click();
			Thread.sleep(1000);
			formularioNuevoPaciente.clickElsewhere();
			Thread.sleep(1000);
			assertThat(formularioNuevoPaciente.getInputErrorText(i)).isEqualTo("Campo obligatorio");
		}
		formularioNuevoPaciente.getGenderInput().click();
		Thread.sleep(1000);
		formularioNuevoPaciente.exitPopup();
		Thread.sleep(1000);
		assertThat(formularioNuevoPaciente.getInputErrorText(3)).isEqualTo("Campo obligatorio");
		for(int i = 4; i < 6; i++) {
			formularioNuevoPaciente.getInput(i-1).click();
			Thread.sleep(1000);
			formularioNuevoPaciente.clickElsewhere();
			Thread.sleep(1000);
			assertThat(formularioNuevoPaciente.getInputErrorText(i)).isEqualTo("Campo obligatorio");
		}
		
		assertTrue(!formularioNuevoPaciente.getSendButton().isEnabled());
		
		formularioNuevoPaciente.getInput(0).sendKeys("00000000X");
		formularioNuevoPaciente.getInput(1).sendKeys("00000000X");
		formularioNuevoPaciente.getInput(2).sendKeys("00000000X");
		formularioNuevoPaciente.getGenderInput().click();
		formularioNuevoPaciente.getGenderOption(0).click();
		formularioNuevoPaciente.getInput(3).sendKeys("2001-01-01");
		formularioNuevoPaciente.getInput(4).sendKeys("00000000X");
		
		assertTrue(formularioNuevoPaciente.getSendButton().isEnabled());
		
	}
	
	@Test
	public void checkClickables() throws InterruptedException {
		
		formularioNuevoPaciente.getReturnButton().click();
		boolean successfulOperation = true;
		try {
			successfulOperation = new WebDriverWait(driver, Duration.ofSeconds(5))
					.until(driver -> driver.getCurrentUrl().equals(RETURN_PAGE_URL));
		}catch(TimeoutException e) {
			successfulOperation = false;
		}
		assertTrue(successfulOperation);
		
		driver.get(FORMULARIO_URL);
		Thread.sleep(1000);
		formularioNuevoPaciente.getHelpButton().click();
		Thread.sleep(1000);
		
		assertThat(formularioNuevoPaciente.getHelpTitle()).isEqualTo("AYUDA");
		Thread.sleep(1000);
		assertThat(formularioNuevoPaciente.getHelpText()).isEqualTo("Rellene los campos para registrar un nuevo paciente en el sistema. Este nuevo paciente se asociará directamente con usted.");
		
		
	}
	
}
