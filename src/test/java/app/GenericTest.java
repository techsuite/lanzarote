package app;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class GenericTest {

  protected WebDriver driver;

  @BeforeClass
  public static void setUp() {
    WebDriverManager.chromedriver().setup();
  }

  @Before
  public void initSession() {
    this.driver = new ChromeDriver();
  }

  @After
  public void closeSession() {
    this.driver.quit();
  }
}
