package app.company.components;

import app.GenericUnit;
import java.util.List;
import java.util.stream.Collectors;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NavBarComponent extends GenericUnit {

	public NavBarComponent(WebDriver driver) {
		super(driver);
	}

	public List<String> getLiterals() {
		return driver.findElements(By.cssSelector("app-nav-bar li")).stream().map(WebElement::getText)
				.collect(Collectors.toList());
	}

	public void clickOn(String option) {
		String optionXPath = String.format("//app-nav-bar//li[text()='%s']", option);
		driver.findElement(By.xpath(optionXPath)).click();
	}
}
