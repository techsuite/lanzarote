package app.company.components;

import app.GenericUnit;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ServicesComponent extends GenericUnit {

	public ServicesComponent(WebDriver driver) {
		super(driver);
	}

	public String getTitle() {
		return driver.findElement(By.xpath("//h1[contains(text(),'Our Services')]")).getText();
	}

	public String getSubtitle() {
		return driver.findElement(By.xpath("//p[contains(text(),'We offer')]")).getText();
	}

	public List<String> getIcons() {
		return driver.findElements(By.xpath("//app-icon-with-title/div")).stream().map(WebElement::getText)
				.collect(Collectors.toList());
	}

	public WebElement getGoogleLogo(String imgPath) {
		String imgXPath = String.format("//img[@src='%s']", imgPath);
		return driver.findElement(By.xpath(imgXPath));
	}

	public String getDescription() {
		return driver
				.findElement(By.xpath("//h2[text()=\"Don't waste your time, hire professionals, hire Techsuite.\"]"))
				.getText();
	}

	public void clickOnGoogleLink(String option) {
		String optionXPath = String.format("//a[contains(text(),'%s')]", option);
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		foo.click();
	}
}
