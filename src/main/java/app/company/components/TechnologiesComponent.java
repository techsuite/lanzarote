package app.company.components;

import app.GenericUnit;
import java.util.List;
import java.util.stream.Collectors;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TechnologiesComponent extends GenericUnit {

	public TechnologiesComponent(WebDriver driver) {
		super(driver);
	}

	public String getTitle() {
		return driver.findElement(By.xpath("//h1[contains(text(),'Our Technologies')]")).getText();
	}

	public String getSubtitle() {
		return driver.findElement(By.xpath("//p[contains(text(),'We offer')]")).getText();
	}

	public String getLine() {
		return driver.findElement(By.xpath("//p[contains(text(),'Some examples')]")).getText();
	}

	public List<String> getIcons() {
		return driver.findElements(By.xpath("//app-icon-with-title/div")).stream().map(WebElement::getText)
				.collect(Collectors.toList());
	}
}
