package app.clinica.testIntegros;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.GenericUnit;
import app.clinica.boston.Helper;

public class IntegridadPacientes extends GenericUnit {

	public IntegridadPacientes(WebDriver driver) {
		super(driver);
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public String getPage() {
		return Helper.fluentElement("//div[@class='numPagPac']/h5").getText();
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public int getTableLength() {
		String palabra = Helper.fluentElement("//table[@id='pacsAsociados']/tbody").getAttribute("childElementCount");
		int longitud = palabra.length();
		int num = 0;
		for(int i = longitud-1; i >= 0; i--)
			num += (palabra.charAt(i)-'0')*(Math.pow(10,longitud-i-1));
		return num;
	}
		
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public List<String> getTableEntry(int nFila) {
		List<String> datos = new ArrayList<>();
		List<WebElement> fila = Helper.fluentElements("//table[@id='pacsAsociados']/tbody/tr["+(nFila+1)+"]/td");
		for(int i = 0; i < 8; i++)
			datos.add(fila.get(i).getText());
		return datos;
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getNextPageButton() {
		WebElement elemento = null;
		try {
			elemento = Helper.fluentElement("//div[@class='numPagPac']/*/b[text()='Siguiente']");
		}catch(TimeoutException e) {}
		return elemento;
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public boolean checkValidData(int nFila, String dni) throws InterruptedException {
		boolean checkType = false;
		boolean checkDNI = false;
		boolean checkMark = false;
		Helper.fluentElement("//table[@id='pacsAsociados']/tbody/tr["+(nFila+1)+"]/td/button/b[1]").click();
		Thread.sleep(1000);
		if(hasConsultas()) {
			String palabra = new WebDriverWait(driver, Duration.ofSeconds(2))
					.until(driver -> driver.findElement(By.xpath("//table[@id='consPrevias']/tbody"))).getAttribute("childElementCount");
			int longitud = palabra.length();
			int num = 0;
			for(int i = longitud-1; i >= 0; i--)
				num += (palabra.charAt(i)-'0')*(Math.pow(10,longitud-i-1));
			WebElement botonAvanzar = getNextPageButtonConsultas();
			while(botonAvanzar != null && !checkMark) {
				for(int i = 0; i < num && !checkMark ; i++) {
					String dato = Helper.fluentElement("//table[@id='consPrevias']/tbody/tr["+(i+1)+"]/td[4]").getText();
					checkType = checkType || dato.matches("pcr.analisis de sangre|analisis de sangre.pcr|pcr|analisis de sangre");
					checkDNI = checkDNI || Helper.fluentElement("//table[@id='consPrevias']/tbody/tr["+(i+1)+"]/td[6]").getText().equalsIgnoreCase(dni);
					checkMark = checkType && checkDNI;
				}
				if(!checkMark) {
					botonAvanzar.click();
					Thread.sleep(1000);
					botonAvanzar = getNextPageButtonConsultas();
					palabra = new WebDriverWait(driver, Duration.ofSeconds(2))
							.until(driver -> driver.findElement(By.xpath("//table[@id='consPrevias']/tbody"))).getAttribute("childElementCount");
					longitud = palabra.length();
					num = 0;
					for(int i = longitud-1; i >= 0; i--)
						num += (palabra.charAt(i)-'0')*(Math.pow(10,longitud-i-1));
				}
			}
			for(int i = 0; i < num && !checkMark ; i++) {
				String dato = Helper.fluentElement("//table[@id='consPrevias']/tbody/tr["+(i+1)+"]/td[4]").getText();
				checkType = checkType || dato.matches("pcr.analisis de sangre|analisis de sangre.pcr|pcr|analisis de sangre");
				checkDNI = checkDNI || Helper.fluentElement("//table[@id='consPrevias']/tbody/tr["+(i+1)+"]/td[6]").getText().equalsIgnoreCase(dni);
				checkMark = checkType && checkDNI;
			}
			WebElement botonRetroceder = getLastPageButtonConsultas();
			while(botonRetroceder!=null) {
				botonRetroceder.click();
				Thread.sleep(1000);
				botonRetroceder = getLastPageButtonConsultas();
			}
		}
		
		return checkMark;
	}
	
	private boolean hasConsultas(){
		boolean check;
		try {
			check = ! new WebDriverWait(driver, Duration.ofSeconds(2))
				.until(driver -> driver.findElement(By.xpath("//h2[text()=' NO TIENE CONSULTAS PREVIAS. ']"))).isDisplayed();
		}catch(Exception e) {
			check = false;
		}
		return check;
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	private WebElement getNextPageButtonConsultas() {
		WebElement elemento = null;
		try {
			elemento = new WebDriverWait(driver, Duration.ofSeconds(2))
					.until(driver -> driver.findElement(By.xpath("//div[@class='numPagCon']/*/b[text()='Siguiente']")));
		}catch(TimeoutException e) {}
		return elemento;
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	private WebElement getLastPageButtonConsultas() {
		WebElement elemento = null;
		try {
			elemento = new WebDriverWait(driver, Duration.ofSeconds(2))
				.until(driver -> driver.findElement(By.xpath("//div[@class='numPagCon']/*/b[text()='Anterior']")));
		}catch(TimeoutException e) {}
		return elemento;
	}
	
}
