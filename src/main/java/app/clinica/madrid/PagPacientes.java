package app.clinica.madrid;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.GenericUnit;

public class PagPacientes extends GenericUnit {

	public PagPacientes(WebDriver driver) {
		super(driver);
	}

	public void login(String user, String pssw) {
		WebElement usernameElement = driver.findElement(By.xpath("//input[@type='text']"));
		WebElement passwordElement = driver.findElement(By.xpath("//input[@type='password']"));

		usernameElement.sendKeys(user);
		passwordElement.sendKeys(pssw);
		driver.findElement(By.xpath("//input[@type='submit']")).click();

	}

	// Usada para comprobar botones de idiomas y "todos los pacientes".
	public void clickOnBotones(String option) {
		String optionXPath = String.format("//button[text()='%s']", option);
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(2))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		foo.click();
	}

	// Usada para clicar en el botón de buscar y cancelar.
	public void clickOnBuscar(String option) {
		String optionXPath = String.format("//*[@id=\"%s\"]",option);
		//*[@id="borrarVarios"]/i     enviarUnico
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(2))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		foo.click();
	}

	// Usada para navegar por la lista de pacientes
	public void clickOnFlecha(String option) {
		String optionXPath = String
				.format("/html/body/app-root/app-anclaje-login/app-pacientes/body/div/div/div/button%s/i", option);
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(2))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		foo.click();
	}


	// Usada para saber cuantas páginas de pacientes hay
	public int numPagsPacientes() {
		String optionXPath = "/html/body/app-root/app-anclaje-login/app-pacientes/body/div/div/div/h5";
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		String texto = foo.getAttribute("innerText");
		String num_en_string = texto.substring(texto.length() - 1);
		return Integer.parseInt(num_en_string);
	}

	// Usada para comprobar el título de mis pacientes
	public String getTitle() {
		String optionXPath = "/html/body/app-root/app-anclaje-login/app-pacientes/body/div/h1";
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		return foo.getAttribute("innerText");
	}

	// Usada para comprobar botones de la barra de navegación
	public void clickOnNavBar(String option) {
		String optionXPath = String.format("//app-nav-bar-element//a[text()='%s']", option);
		driver.findElement(By.xpath(optionXPath)).click();
	}

	// Usada para comprobar botón de desconexión
	public void clickDesconecta() {
		String optionXPath = "/html/body/app-root/app-anclaje-login/app-pacientes/app-nav-bar/div/button/i";
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(2))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		foo.click();
	}

}
