package app.clinica.madrid;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.GenericUnit;

public class Impostores extends GenericUnit {

	public Impostores(WebDriver driver) {
		super(driver);
	}

	// Login general
	public void login(String user, String pssw) {
		WebElement usernameElement = driver.findElement(By.xpath("//input[@type='text']"));
		WebElement passwordElement = driver.findElement(By.xpath("//input[@type='password']"));

		usernameElement.sendKeys(user);
		passwordElement.sendKeys(pssw);
		driver.findElement(By.xpath("//input[@type='submit']")).click();

	}

	// Introducir clave si hemos introducido un impostor
	public void loginClave(String clave) {
		WebElement claveElement = driver
				.findElement(By.xpath("/html/body/app-root/ng-component/app-center-layout/div/form/div/div/input[1]"));

		claveElement.sendKeys(clave);
		driver.findElement(By.xpath("/html/body/app-root/ng-component/app-center-layout/div/form/div/div/input[2]"))
				.click();

	}

	// Comprueba el icono de bienvenida
	public String getIcono() {
		String optionXPath = "/html/body/app-root/app-anclaje-login/app-bienvenida/app-center-layout/div/div/div[1]/img";
		WebElement icono = new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		return icono.getAttribute("src");
	}

	// Usada para comprobar botón de desconexión desde página bienvenida
	public void clickDesconecta1() {
		String optionXPath = "/html/body/app-root/app-anclaje-login/app-bienvenida/app-nav-bar/div/button";
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(2))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		foo.click();
	}

	// Usada para comprobar botón de desconexión despues del login antes de la clave
	public void clickDesconecta2() {
		String optionXPath = "/html/body/app-root/ng-component/app-nav-bar/div/button/i";
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(2))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		foo.click();
	}

	// Usada para clicar en "iniciar sesión" en loginAgain
	public void clickInicia() {
		String optionXPath = "/html/body/app-root/ng-component/app-center-layout/div/div/div/button";
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(2))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		foo.click();
	}

	// Usada para comprobar botones de la barra de navegación
	public void clickOnNavBar(String option) {
		String optionXPath = String.format("//app-nav-bar-element//a[text()='%s']", option);
		driver.findElement(By.xpath(optionXPath)).click();
	}

	// Usada para comprobar el título de Inicio de sesión
	public String getTitle() {
		String optionXPath = "/html/body/app-root/ng-component/app-center-layout/div/form/h1";
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		return foo.getAttribute("innerText");
	}

	// Usada para comprobar el título de Inicio de sesión
	public String getButtonText() {
		String optionXPath = "/html/body/app-root/ng-component/app-center-layout/div/form/div/div/input[2]";
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		return foo.getAttribute("value");
	}

	// Usada para comprobar el texto del input de la clave 
	public String getInputText() {
		String optionXPath = "/html/body/app-root/ng-component/app-center-layout/div/form/div/div/input[1]";
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		return foo.getAttribute("placeholder");
	}

}
