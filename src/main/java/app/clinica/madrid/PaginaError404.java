package app.clinica.madrid;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import app.GenericUnit;
import app.clinica.dakar.Helper;

public class PaginaError404 extends GenericUnit {

	public PaginaError404(WebDriver driver) {
		super(driver);
	}

	private String getError404Header() {
		return Helper.fluentElement("//h1[text()='Página no encontrada']").getText();
	}

	public WebElement getButton() {
		return Helper.fluentElement("//button[@class = 'btn btn-primary button']");
	}

	public boolean inPaginaError404() {
		return getError404Header().equals("Página no encontrada") && getButton().getText().equals("Ir a página de inicio");
	}
}
