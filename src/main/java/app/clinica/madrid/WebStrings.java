package app.clinica.madrid;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.GenericUnit;
import app.clinica.dakar.Helper;

public class WebStrings extends GenericUnit {

	public WebStrings(WebDriver driver) {
		super(driver);
	}
	
	//Esta funcion sale de PagTrabajadores
	public void login(String user, String pssw) {
		WebElement usernameElement = driver.findElement(By.xpath("//input[@type='text']"));
		WebElement passwordElement = driver.findElement(By.xpath("//input[@type='password']"));

		usernameElement.sendKeys(user);
		passwordElement.sendKeys(pssw);
		driver.findElement(By.xpath("//input[@type='submit']")).click();

	}
	
	public List<String> getAllStrings() {
		List<String> res = new ArrayList<>();
		List<String> parrafos = getElementTexts("//p[text()]");
		List<String> botones = getElementTexts("//button[text()]");
		List<String> aes = getElementTexts("//a[text()]");
		List<String> h5s = getElementTexts("//h5[text()]");
		List<String> options = getElementTexts("//option[text()]");
		List<String> h1s = getElementTexts("//h1[text()]");
		List<String> ths = getElementTexts("//th[text()]");
		List<String> h3s = getElementTexts("//h3[text()]");
		res.addAll(parrafos);
		res.addAll(botones);
		res.addAll(aes);
		res.addAll(h5s);
		res.addAll(options);
		res.addAll(h1s);
		res.addAll(ths);
		res.addAll(h3s);
		return res;

	}
	
	public void changeLang(String lang) throws InterruptedException {
		if (lang.equals("ES")) {
			new WebDriverWait(driver, Duration.ofSeconds(5))
					.until(driver -> driver.findElement(By.xpath("//button[@class='spanishButton']"))).click();
			Thread.sleep(200);
		} else {
			new WebDriverWait(driver, Duration.ofSeconds(5))
					.until(driver -> driver.findElement(By.xpath("//button[@class='englishButton']"))).click();
			Thread.sleep(200);
		}
	}
	
	private List<String> getElementTexts(String xpath) {
		return driver.findElements(By.xpath(xpath)).stream().map(WebElement::getText).collect(Collectors.toList());
	}
	
}
