package app.clinica.madrid;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.GenericUnit;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;

public class PagTrabajadores extends GenericUnit {

	public PagTrabajadores(WebDriver driver) {
		super(driver);
	}

	public void login(String user, String pssw) {
		WebElement usernameElement = driver.findElement(By.xpath("//input[@type='text']"));
		WebElement passwordElement = driver.findElement(By.xpath("//input[@type='password']"));

		usernameElement.sendKeys(user);
		passwordElement.sendKeys(pssw);
		driver.findElement(By.xpath("//input[@type='submit']")).click();

	}

	//Usada para comprobar botones 'Pacientes' y 'Techsuite-Wiki' y los de idiomas.
	public void clickOnBotones(String option) {
		String optionXPath = String.format("//button[text()='%s']", option);
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(2))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		foo.click();
	}

	//Usada para comprobar botón de desconexión
	public void clickDesconecta() {
		String optionXPath = "/html/body/app-root/app-anclaje-login/app-bienvenida/app-nav-bar/div/button";
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(2))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		foo.click();
	}

	//Usada para comprobar botones de la barra de navegación
	public void clickOnNavBar(String option) {
		String optionXPath = String.format("//app-nav-bar-element//a[text()='%s']", option);
		driver.findElement(By.xpath(optionXPath)).click();
	}
    
	//Usada para comprobar el título de bienvenida
	public String getTitle() {
		String optionXPath = "/html/body/app-root/app-anclaje-login/app-bienvenida/app-center-layout/div/div/h1[1]";
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		return foo.getAttribute("innerText");
	}
	
	//Comprueba la existencia y que sea la imagen correcta de los iconos.
	public String getIconos(String option) {
		String optionXPath = String.format("//img[@alt='%s']",option);
		WebElement foo = new WebDriverWait(driver, Duration.ofSeconds(5))
				.until(driver -> driver.findElement(By.xpath(optionXPath)));
		return foo.getAttribute("src");
	}
	
	

}
