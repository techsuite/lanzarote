package app.clinica.dakar;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.GenericUnit;

public class PaginaComparacion extends GenericUnit {

	WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(1));

	public PaginaComparacion(WebDriver driver) {
		super(driver);
	}

	public List<String> getHeaders() {
		return Helper.fluentElements("//div[@class='card-header']").stream().map(WebElement::getText)
				.collect(Collectors.toList());
	}

	public List<String> getFields() {
		return Helper.fluentElements("//h2").stream().map(WebElement::getText).collect(Collectors.toList());
	}

	public List<String> getTexts() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		return Helper.fluentElements("//h5").stream().map(WebElement::getText).collect(Collectors.toList());
	}

	public List<WebElement> getMoreInfoButtons() {
		List<WebElement> tmp = Helper.fluentElements("//button[@class='button-info']");
		for (WebElement we : tmp)
			wait.until(ExpectedConditions.elementToBeClickable(we));
		return tmp;
	}

}
