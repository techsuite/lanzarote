package app.clinica.dakar;

import app.GenericUnit;
import java.util.List;
import java.util.stream.Collectors;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BienvenidaPacientes extends GenericUnit {

	public BienvenidaPacientes(WebDriver driver) {
		super(driver);
	}

	public String getTitle() {
		return driver.findElement(By.xpath("//h1[contains(text(),'Bienvenido')]")).getText();
	}

	public String getText() {
		return driver.findElement(By.xpath("//p[contains(text(),'La Clínica April')]")).getText();
	}

	public void clickOn(String option) {
		String optionXPath = String.format("//button[text()='%s']", option);
		driver.findElement(By.xpath(optionXPath)).click();
	}

	public void clickOnNavBar(String option) {
		String optionXPath = String.format("//app-nav-bar-element//a[text()='%s']", option);
		driver.findElement(By.xpath(optionXPath)).click();
	}

	public WebElement getTechsuiteLogo(String imgPath) {
		String imgXPath = String.format("//img[@src='%s']", imgPath);
		return driver.findElement(By.xpath(imgXPath));
	}

	public List<String> getLiterals() {
		return driver.findElements(By.cssSelector("app-nav-bar a")).stream().map(WebElement::getText)
				.collect(Collectors.toList());
	}
}
