package app.clinica.dakar;

import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import app.GenericUnit;


public class LoginPacientes extends GenericUnit{

	public LoginPacientes(WebDriver driver) {
		super(driver);
	}
	
	public String getTitle() {
		return driver.findElement(By.xpath("//h4[contains(text(),'Login')]")).getText();
	}
	
	public String getErrorMsg() {
		return driver.findElement(By.xpath("//strong[text()='incorrecta']")).getText();
	}
	
	public void clickOn(String option) {
	    String optionXPath = String.format(
	      "//button[text()='%s']",
	      option
	    );
	    driver.findElement(By.xpath(optionXPath)).click();
	}
	
	public void clickOnNavBar(String option) {
	    String optionXPath = String.format(
	      "//app-nav-bar-element//a[text()='%s']",
	      option
	    );
	    driver.findElement(By.xpath(optionXPath)).click();
	}
	
	public WebElement getTechsuiteLogo(String imgPath){
		  String imgXPath = String.format(
			      "//img[@src='%s']",
			      imgPath
			    );
		  return driver.findElement(By.xpath(imgXPath));
	  }
	
	public List<String> getLiterals() {
	    return driver
	      .findElements(By.cssSelector("app-nav-bar a"))
	      .stream()
	      .map(WebElement::getText)
	      .collect(Collectors.toList());
	  }
	
	public void login(String user, String pssw) {
		WebElement usernameElement = driver.findElement(By.xpath("//input[@type='text']"));
		WebElement passwordElement = driver.findElement(By.xpath("//input[@type='password']"));
		
		usernameElement.sendKeys(user);
		passwordElement.sendKeys(pssw);
		driver.findElement(By.xpath("//button[text()='Entrar ']")).click();
	}

}
