package app.clinica.dakar;

import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import app.GenericUnit;

public class PaginaConsultasAnalisis extends GenericUnit {

	public PaginaConsultasAnalisis(WebDriver driver) {
		super(driver);
		Helper.init(driver);
	}

	public WebElement getFilterBox() {
		return Helper.fluentElements("//input").get(0);
	}

	/*
	 * 1) Fecha, 2) Doctor, 3) DNI Doctor
	 */
	public List<String> getColumn(String number) {
		List<WebElement> col = Helper
				.fluentElements(String.format("//table[@class = 'table table-striped']//tr//td[%s]", number));
		return col.stream().map(WebElement::getText).collect(Collectors.toList());
	}

	public void openMenuButton() {
		WebElement button = Helper.fluentElement("//img[@src = 'assets/mas.ico']");
		button.click();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void getMenuButtonClick(String option, boolean waitForRedirect) {
		String previousUrl = driver.getCurrentUrl();
		WebElement button = null;
		switch(option) {
		case "wiki":
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}
			button = Helper.fluentElement("//button[@class='boton1']");
			break;
		case "consultas":
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}
			button = Helper.fluentElement("//button[@class='boton3']");
			break;
		case "analisis":
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}
			button = Helper.fluentElement("//button[@class='boton2']");
			break;
		}
		button.click();

		if (waitForRedirect)
			Helper.fluentUrlChanged(previousUrl);
	}

	public void setFilterOptions(int campo) {
		Select dropdown = new Select(Helper.fluentElement("//select[@name='select']"));
		dropdown.selectByIndex(campo);
	}

	public void clickSearchButton() {
		Helper.fluentElement("//i[@class='fa fa-search']").click();
	}

	public void selectMonth(String s) {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
		}
		Select dropdown = new Select(Helper.fluentElements("//select[@class='custom-select']").get(0));
		dropdown.selectByIndex(Integer.parseInt(s) - 1);
	}

	public void selectYear(String s) {
		Select dropdown = new Select(Helper.fluentElements("//select[@class='custom-select']").get(1));
		dropdown.selectByIndex(Integer.parseInt(s) - 2011);
	}

	public void selectday(String string) {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
		}
		WebElement day = Helper.fluentElement(String.format("//div[text()='%s']",string));
		day.click();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
		}
	}

	public List<WebElement> getBotonesTabla() {
		return Helper.fluentElements("//td/button");
	}
}
