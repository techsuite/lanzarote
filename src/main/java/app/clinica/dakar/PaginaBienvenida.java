package app.clinica.dakar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import app.GenericUnit;

public class PaginaBienvenida extends GenericUnit {

	public PaginaBienvenida(WebDriver driver) {
		super(driver);
		Helper.init(driver);
	}

	public void getButtonClick(String option) {
		String previousUrl = driver.getCurrentUrl();
		WebElement button;
		if (option.equals("consultas"))
			button = Helper.fluentElement("//button[@class = 'boton1']");
		else
			button = Helper
					.fluentElement(String.format("//div[@class='botones']//button[contains(text(),'%s')]", option));
		button.click();

		Helper.fluentUrlChanged(previousUrl);
	}

	public void clickSignOutButton() {
		String previousUrl = driver.getCurrentUrl();
		WebElement button = Helper.fluentElement("//i[@class = 'fa fa-sign-out']");
		button.click();

		Helper.fluentUrlChanged(previousUrl);
	}
}