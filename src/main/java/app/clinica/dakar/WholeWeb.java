package app.clinica.dakar;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import app.GenericUnit;

public class WholeWeb extends GenericUnit {

	public WholeWeb(WebDriver driver) {
		super(driver);
		Helper.init(driver);
	}

	public List<String> getAllStrings() {
		List<String> res = new ArrayList<>();
		List<String> parrafos = getElementTexts("//p[text()]");
		List<String> botones = getElementTexts("//button[text()]");
		List<String> aes = getElementTexts("//a[text()]");
		List<String> h5s = getElementTexts("//h5[text()]");
		List<String> options = getElementTexts("//option[text()]");
		List<String> h1s = getElementTexts("//h1[text()]");
		List<String> ths = getElementTexts("//th[text()]");
		List<String> h3s = getElementTexts("//h3[text()]");
		res.addAll(parrafos);
		res.addAll(botones);
		res.addAll(aes);
		res.addAll(h5s);
		res.addAll(options);
		res.addAll(h1s);
		res.addAll(ths);
		res.addAll(h3s);
		return res;

	}

	public void changeLang(String lang) throws InterruptedException {
		if (lang.equals("ES")) {
			Helper.fluentElement("//button[@class='spanishButton']").click();
			Thread.sleep(200);
		} else {
			Helper.fluentElement("//button[@class='englishButton']").click();
			Thread.sleep(200);
		}
	}

	private List<String> getElementTexts(String xpath) {
		return driver.findElements(By.xpath(xpath)).stream().map(WebElement::getText).collect(Collectors.toList());
	}

}
