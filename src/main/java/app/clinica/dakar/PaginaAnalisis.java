package app.clinica.dakar;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import app.GenericUnit;
import app.clinica.managua.Helper;
import app.clinica.managua.PruebaAS;
import app.clinica.managua.PruebaPCR;

public class PaginaAnalisis extends GenericUnit {

	public PaginaAnalisis(WebDriver driver) {
		super(driver);
		Helper.init(driver);
	}

	public WebElement getFilterBox() {
		return Helper.fluentElement("//input[@name = 'filterEntry']");
	}

	/*
	 * 1) Fecha, 2) Doctor, 3) Especialidad, 4) Tipo Prueba
	 */
	public List<String> getColumn(String number) {
		List<WebElement> col = Helper
				.fluentElements(String.format("//table[@class = 'table table-striped']//tr//td[%s]", number));
		return col.stream().map(WebElement::getText).collect(Collectors.toList());
	}

	public List<PruebaAS> getAS() throws InterruptedException {
		Thread.sleep(1500);
		List<PruebaAS> res = new ArrayList<>();
		List<String> tipos = Helper.fluentElements("//table[@class='table table-striped']//td[2]").stream()
				.map(WebElement::getText).map(String::trim).collect(Collectors.toList());
		int[] posBuscadas = IntStream.range(0, tipos.size()).filter(i -> tipos.get(i).equals("AnalisisSangre"))
				.toArray();

		String fecha, tipo, glucosa, colesterol, trigliceridos, bilirrubina;
		String prev;
		for (Integer i : posBuscadas) {
			Thread.sleep(1000);
			Helper.fluentElement("(//table[@class='table table-striped']//td[3])[" + (i + 1) + "]").click();
			Thread.sleep(1000);
			fecha = Helper.fluentElement("//p[contains(text(),'Fecha')]").getText().split(" ")[3];
			tipo = "AnalisisSangre";
			glucosa = Helper.fluentElement("//p[contains(text(),'Glucosa')]").getText().split(" ")[1];
			colesterol = Helper.fluentElement("//p[contains(text(),'Colesterol')]").getText().split(" ")[1];
			trigliceridos = Helper.fluentElement("//p[contains(text(),'Triglicéridos')]").getText().split(" ")[1];
			bilirrubina = Helper.fluentElement("//p[contains(text(),'Bilirrubina')]").getText().split(" ")[1];
			res.add(new PruebaAS("", "", fecha, tipo, glucosa, colesterol, trigliceridos, bilirrubina));
			prev = driver.getCurrentUrl();
			driver.navigate().back();
			Helper.fluentUrlChanged(prev);

		}
		return res;
	}

	public List<PruebaPCR> getPCR() throws InterruptedException {
		Thread.sleep(1500);
		List<PruebaPCR> res = new ArrayList<>();
		List<String> tipos = Helper.fluentElements("//table[@class='table table-striped']//td[2]").stream()
				.map(WebElement::getText).map(String::trim).collect(Collectors.toList());
		int[] posBuscadas = IntStream.range(0, tipos.size()).filter(i -> tipos.get(i).equals("PCR")).toArray();

		String fecha, fluorescencia;
		String prev;
		for (Integer i : posBuscadas) {
			Thread.sleep(1000);
			Helper.fluentElement("(//table[@class='table table-striped']//td[3])[" + (i + 1) + "]").click();
			Thread.sleep(1000);
			fecha = Helper.fluentElement("//p[contains(text(),'Fecha')]").getText().split(" ")[3];
			fluorescencia = Helper.fluentElement("//p[contains(text(),'Fluorescencia')]").getText().split(" ")[1];
			res.add(new PruebaPCR("", "PCR", fecha, "", fluorescencia));
			prev = driver.getCurrentUrl();
			driver.navigate().back();
			Helper.fluentUrlChanged(prev);
		}
		return res;
	}
}
