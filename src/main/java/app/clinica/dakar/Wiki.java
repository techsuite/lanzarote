package app.clinica.dakar;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.GenericUnit;

public class Wiki extends GenericUnit {

	WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(1));

	public Wiki(WebDriver driver) {
		super(driver);
		Helper.init(driver);
	}

	public WebElement getInput1() {
		List<WebElement> multiple = getInputMultiple();
		if (multiple.size() == 0) {
			WebElement inputUnico = Helper.fluentElement("//input[@id = 'inputUnico']");
			wait.until(ExpectedConditions.elementToBeClickable(inputUnico));
			return inputUnico;
		}
		return multiple.get(0);
	}

	public WebElement getInput2() {
		return getInputMultiple().get(1);
	}

	public List<WebElement> getInputMultiple() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		List<WebElement> tmp = Helper.fluentElements("//input[@id = 'inputVarios']");
		for (WebElement we : tmp)
			wait.until(ExpectedConditions.elementToBeClickable(we));
		return tmp;
	}

	public WebElement getRemoveButton1() {
		return getRemoveButtons().get(0);
	}

	public WebElement getRemoveButton2() {
		return getRemoveButtons().get(0);
	}

	public List<WebElement> getRemoveButtons() {
		List<WebElement> tmp = Helper.fluentElements("//i[@class='fa fa-times']");
		for (WebElement we : tmp)
			wait.until(ExpectedConditions.elementToBeClickable(we));
		return tmp;
		// .stream().filter(WebElement::isDisplayed).collect(Collectors.toList())
	}

	public WebElement getAddButton1() {
		return getAddButtons().get(0);
	}

	public WebElement getAddButton2() {
		return getAddButtons().get(1);
	}

	public List<WebElement> getAddButtons() {
		List<WebElement> tmp = Helper.fluentElements("//i[@class='fa fa-plus']");
		for (WebElement we : tmp)
			wait.until(ExpectedConditions.elementToBeClickable(we));
		return tmp;
	}

	public WebElement getSendButton() {
		return Helper.fluentElement("//i[@class='fa fa-send']");
	}

	public boolean sendButtonVisible() {
		try {
			WebElement tmp = getSendButton();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public WebElement getCompareButton() {
		return wait.until(
				ExpectedConditions.elementToBeClickable(Helper.fluentElement("//button[@class = 'buttonCompare']")));
	}
}
