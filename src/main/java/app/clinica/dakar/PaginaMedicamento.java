package app.clinica.dakar;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.GenericUnit;

public class PaginaMedicamento extends GenericUnit {

	WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(1));

	public PaginaMedicamento(WebDriver driver) {
		super(driver);
	}

	public String getHeader() {
		return Helper.fluentElement("//h1").getText();
	}

	public List<String> getFields() {
		return Helper.fluentElements("//h2").stream().map(WebElement::getText).collect(Collectors.toList());
	}

	public List<String> getTexts() {
		return Helper.fluentElements("//h5").stream().map(WebElement::getText).collect(Collectors.toList());
	}

	public WebElement getMoreInfoButton() {
		WebElement tmp = Helper.fluentElement("//button[@class='button-ayuda']");
		wait.until(ExpectedConditions.elementToBeClickable(tmp));
		return tmp;
	}

}
