package app.clinica.managua;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import app.GenericUnit;

public class IngresoNuevoPaciente extends GenericUnit {
	
	public IngresoNuevoPaciente(WebDriver driver) {
		super(driver);
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public String getTitle(){
		return Helper.fluentElement("//h1[text() ='Datos para el paciente a ingresar']").getText();
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getLogoutButton() {
		return Helper.fluentElement("//button[text()='Desconectarse']");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getInsertButton() {
		return Helper.fluentElement("//button[text()='Guardar paciente']");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getInsertAndStartButton() {
		return Helper.fluentElement("//button[text()='Guardar y empezar prueba']");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getReturnButton() {
		return Helper.fluentElement("//button[@class='botonVolver']");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public String getReturnButtonText() {
		return Helper.fluentElement("//button[@class='botonVolver']/h3").getText();
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public String getLabelText(String name) {
		return Helper.fluentElement("//label[text() ='"+name+"']").getText();
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getInput(String name) {
		return Helper.fluentElement("//label[text() ='"+name+"']/input");
	}
	
	public WebElement getGenderSelection() {
		return Helper.fluentElement("//select[@name='genero']");
	}
	
	public WebElement getGenderOption(String name) {
		return Helper.fluentElement("//option[text()='"+name+"']");
	}

}
