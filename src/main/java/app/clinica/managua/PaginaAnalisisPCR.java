package app.clinica.managua;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import app.GenericUnit;

public class PaginaAnalisisPCR extends GenericUnit{

	public PaginaAnalisisPCR(WebDriver driver) {
		super(driver);
		Helper.init(driver);
	}
	
	public List<PruebaPCR> getPCRFinalizadasByDNI(String dni) throws InterruptedException {
		List<PruebaPCR> res = new ArrayList<>();
		Helper.scrollToBottom();
		Thread.sleep(1000);
		int nPags = Integer.parseInt(Helper.fluentElement("//h5").getText().trim().split("/")[1]);
		List<String> dnis = Helper.fluentElements("//table[@id='PCRFinalizado']//td[3]").stream()
				.map(WebElement::getText).collect(Collectors.toList());
		int[] posBuscadas = IntStream.range(0, dnis.size()).filter(i -> dni.equals(dnis.get(i))).toArray();
		res.addAll(getPruebasByPos(posBuscadas));
		for (int j = 0; j < nPags - 1; j++){
			Helper.scrollToBottom();
			Helper.fluentElement("//b[text()='Siguiente -> ']").click();
			Thread.sleep(1500);
			List<String> dnisAux = Helper.fluentElements("//table[@id='PCRFinalizado']//td[3]").stream()
					.map(WebElement::getText).collect(Collectors.toList());
			posBuscadas = IntStream.range(0, dnisAux.size()).filter(i -> dni.equals(dnisAux.get(i))).toArray();
			res.addAll(getPruebasByPos(posBuscadas));
		}
		return res;
	}

	private List<PruebaPCR> getPruebasByPos(int[] posiciones) throws InterruptedException {
		List<PruebaPCR> res = new ArrayList<>();
		String id, trabajador, fecha, tipo, fluorescencia;
		WebElement boton;
		Actions action = new Actions(driver);
		for (Integer i : posiciones) {
			id = Helper.fluentElement("(//table[@id='PCRFinalizado']//td[1])[" + (i+1) + "]").getText();
			trabajador = Helper.fluentElement("(//table[@id='PCRFinalizado']//td[2])[" + (i+1) + "]").getText();
			fecha = Helper.fluentElement("(//table[@id='PCRFinalizado']//td[4])[" + (i+1) + "]").getText().split(" ")[0];
			tipo = "PCR";
			boton = Helper.fluentElement("(//table[@id='PCRFinalizado']//td[5])[" + (i+1) + "]");
			boton.click();
			Thread.sleep(350);
			fluorescencia = Helper.fluentElement("//div[contains(text(),'Fluorescencia')]").getText().split(" ")[1];
			res.add(new PruebaPCR(id, tipo, fecha, trabajador, fluorescencia));
			action.sendKeys(Keys.ESCAPE).build().perform();
			Thread.sleep(350);
		}
		return res;
	}

}
