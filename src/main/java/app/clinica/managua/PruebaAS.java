package app.clinica.managua;

public class PruebaAS {

	String id, trabajador, fecha, tipo, glucosa, colesterol, trigliceridos, bilirrubina;

	public PruebaAS(String id, String trabajador, String fecha, String tipo, String glucosa, String colesterol,
			String trigliceridos, String bilirrubina) {
		this.id = id;
		this.trabajador = trabajador;
		this.fecha = fecha;
		this.tipo = tipo;
		this.glucosa = glucosa;
		this.colesterol = colesterol;
		this.trigliceridos = trigliceridos;
		this.bilirrubina = bilirrubina;
	}

	public String getId() {
		return id;
	}

	public String getTrabajador() {
		return trabajador;
	}

	public String getFecha() {
		return fecha;
	}

	public String getTipo() {
		return tipo;
	}

	public String getGlucosa() {
		return glucosa;
	}

	public String getColesterol() {
		return colesterol;
	}

	public String getTrigliceridos() {
		return trigliceridos;
	}

	public String getBilirrubina() {
		return bilirrubina;
	}
	
	

	@Override
	public String toString() {
		return "PruebaAS [id=" + id + ", trabajador=" + trabajador + ", fecha=" + fecha + ", tipo=" + tipo
				+ ", glucosa=" + glucosa + ", colesterol=" + colesterol + ", trigliceridos=" + trigliceridos
				+ ", bilirrubina=" + bilirrubina + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PruebaAS other = (PruebaAS) obj;
		if (bilirrubina == null) {
			if (other.bilirrubina != null)
				return false;
		} else if (!bilirrubina.equals(other.bilirrubina))
			return false;
		if (colesterol == null) {
			if (other.colesterol != null)
				return false;
		} else if (!colesterol.equals(other.colesterol))
			return false;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (glucosa == null) {
			if (other.glucosa != null)
				return false;
		} else if (!glucosa.equals(other.glucosa))
			return false;
		if (tipo == null) {
			if (other.tipo != null)
				return false;
		} else if (!tipo.equals(other.tipo))
			return false;
		if (trigliceridos == null) {
			if (other.trigliceridos != null)
				return false;
		} else if (!trigliceridos.equals(other.trigliceridos))
			return false;
		return true;
	}

}
