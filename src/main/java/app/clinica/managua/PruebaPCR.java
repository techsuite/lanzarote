package app.clinica.managua;

public class PruebaPCR {

	String id, tipo, fecha, trabajador, fluorescencia;

	public PruebaPCR(String id, String tipo, String fecha, String trabajador, String fluorescencia) {
		this.id = id;
		this.tipo = tipo;
		this.fecha = fecha;
		this.trabajador = trabajador;
		this.fluorescencia = fluorescencia;
	}

	public String getId() {
		return id;
	}

	public String getTipo() {
		return tipo;
	}

	public String getFecha() {
		return fecha;
	}

	public String getTrabajador() {
		return trabajador;
	}

	public String getFluorescencia() {
		return fluorescencia;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PruebaPCR other = (PruebaPCR) obj;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (fluorescencia == null) {
			if (other.fluorescencia != null)
				return false;
		} else if (!fluorescencia.equals(other.fluorescencia))
			return false;
		return true;
	}

}
