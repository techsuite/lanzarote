package app.clinica.managua;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.GenericUnit;

public class LoginManagua extends GenericUnit{

	public LoginManagua(WebDriver driver) {
		super(driver);
	}
	
	public String getTextLoginDNI() {
		return driver.findElement(By.xpath("//h3[text()='DNI:']")).getText();
	}
	
	public String getTextLoginPassword() {
		return driver.findElement(By.xpath("//h3[text()='Password:']")).getText();
	}
	
	public void login(String dni, String pass) {
		WebElement dniElement = driver.findElement(By.xpath("//input[@id='dni']"));
		WebElement passwordElement = driver.findElement(By.xpath("//input[@id='password']"));

		dniElement.sendKeys(dni);
		passwordElement.sendKeys(pass);
		driver.findElement(By.xpath("//button[@class='botonAcceder']")).click();
	}
	
	public boolean catchErrorMessage(String loginUrl) {
		boolean testOutput = true;
		
		try {
			new WebDriverWait(driver, Duration.ofSeconds(5))
					.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()=' Reintentar ']")));
		} catch (TimeoutException e) {
			testOutput = driver.getCurrentUrl().equals(loginUrl);
		}
		return testOutput;
	}
	
}
