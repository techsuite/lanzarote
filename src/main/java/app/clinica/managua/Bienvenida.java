package app.clinica.managua;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import app.GenericUnit;

public class Bienvenida extends GenericUnit{

	public Bienvenida(WebDriver driver) {
		super(driver);
	}
	
	public String getTitle() {
		return driver.findElement(By.xpath("//h1[contains(text(),'Saludos')]")).getText();
	}
	
	public void clickOn(String option) {
		String optionXPath = String.format("//button[text()='%s']", option);
		driver.findElement(By.xpath(optionXPath)).click();
	}
	
	public void login(String user, String pssw) {
		WebElement usernameElement = driver.findElement(By.xpath("//input[@type='text']"));
		WebElement passwordElement = driver.findElement(By.xpath("//input[@type='password']"));
		
		usernameElement.sendKeys(user);
		passwordElement.sendKeys(pssw);
		driver.findElement(By.xpath("//button[@class='botonAcceder']")).click();
	}

}
