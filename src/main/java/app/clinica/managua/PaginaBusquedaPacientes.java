package app.clinica.managua;

import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import app.GenericUnit;

public class PaginaBusquedaPacientes extends GenericUnit {

	public PaginaBusquedaPacientes(WebDriver driver) {
		super(driver);
		Helper.init(driver);
	}

	/*
	 * Segun el modo elegimos apellido, nombre o dni buscamos y esperamos
	 */
	public void filtrar(String modo, String filtro) {
		WebElement inputBox = null, boton = null;
		switch (modo) {
		case "dni":
			inputBox = Helper.fluentElement("//input[@id='dni']");
			boton = Helper.fluentElement("//button[@id='buscarDni']");
			break;
		case "nombre":
			inputBox = Helper.fluentElement("//input[@id='nombre']");
			boton = Helper.fluentElement("//button[@id='buscarNombre']");
			break;
		case "apellidos":
			inputBox = Helper.fluentElement("//input[@id='apellidos']");
			boton = Helper.fluentElement("//button[@id='buscarApellidos']");
			break;
		}

		inputBox.sendKeys(filtro);
		boton.click();

		try {
			Thread.sleep(600);
		} catch (InterruptedException e) {
		}
	}

	/*
	 * Devolvemos todos los botones para añadir analisis de los pacientes
	 * encontrados
	 */
	public List<WebElement> getBotonesAddAnalisis() {
		return Helper.fluentElements("//button[@id='btn-consultas']");
	}

	public List<String> getDnis() {
		return Helper.fluentElements("//table[@id='resultado']//td[1]").stream().map(WebElement::getText)
				.collect(Collectors.toList());
	}

	public List<String> getNombres() {
		return Helper.fluentElements("//table[@id='resultado']//td[2]").stream().map(WebElement::getText)
				.collect(Collectors.toList());
	}

	public List<String> getApellidos() {
		return Helper.fluentElements("//table[@id='resultado']//td[3]").stream().map(WebElement::getText)
				.collect(Collectors.toList());
	}

	/**
	 * Iniciamos todas las prubeas
	 * 
	 * @return numero de pruebas inciadas
	 */
	public int iniciarTodasPruebas() {
		int i = 0;
		for (WebElement we : getBotonesAddAnalisis()) {
			Helper.scrollToBottom();
			we.click();
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
			}
			Helper.fluentElement("//button[text()='Sí, adelante']").click();
			Helper.fluentElement("//button[text()='OK']").click();
			i++;
		}
		return i;
	}

	public void iniciarUnaPrueba() {
		WebElement boton = getBotonesAddAnalisis().get(0);
		Helper.scrollToBottom();
		boton.click();
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
		}
		Helper.fluentElement("//button[text()='Sí, adelante']").click();
		Helper.fluentElement("//button[text()='OK']").click();
	}

	/*
	 * Boton para retroceder y esperar a que cambie la pagina
	 */
	public void volver() {
		String prev = driver.getCurrentUrl();
		Helper.scrollToTop();
		Helper.fluentElement("//button[text()='Volver']").click();
		Helper.fluentUrlChanged(prev);
	}
}
