package app.clinica.managua;

import java.time.Duration;
import java.util.List;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Helper {

	private static Wait<WebDriver> wait;
	private static WebDriver driver;

	public static void init(WebDriver drv) {
		driver = drv;
		wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10)).pollingEvery(Duration.ofSeconds(1))
				.ignoring(NoSuchElementException.class).ignoring(ElementNotInteractableException.class);
	}

	/**
	 * Returns the WebElement of a given xpath waiting fluently for it or throwing
	 * an exception if the wait times-out
	 * 
	 * @param xpath
	 * @return
	 */
	public static WebElement fluentElement(String xpath) {
		return wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(By.xpath(xpath));
			}
		});
	}

	public static List<WebElement> fluentElements(String xpath) {
		return wait.until(new Function<WebDriver, List<WebElement>>() {
			public List<WebElement> apply(WebDriver driver) {
				return driver.findElements(By.xpath(xpath));
			}
		});
	}

	public static void fluentUrlChanged(String previousUrl) throws TimeoutException {
		new WebDriverWait(driver, Duration.ofSeconds(5)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return !previousUrl.equals(driver.getCurrentUrl());
			}
		});
	}

	public static void waitForElementVisible(String xpath) throws TimeoutException {
		new WebDriverWait(driver, Duration.ofSeconds(10))
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
	}

	public static void waitForElementInteractable(String xpath) throws TimeoutException {
		new WebDriverWait(driver, Duration.ofSeconds(10))
				.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
	}

	public static void scrollToBottom() {
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	public static void scrollToTop() {
		((JavascriptExecutor) driver).executeScript("window.scrollTo(document.body.scrollHeight,0)");
	}

	/**
	 * Login method
	 * 
	 * @param user
	 * @param password
	 * @throws TimeoutException: Fails when no redirect happens
	 */
	public static void login(String usr, String passw) {
		WebElement usrBox = fluentElement("//input[@id='dni']");
		WebElement passBox = fluentElement("//input[@id='password']");
		WebElement enterButton = fluentElement("//button[@class='botonAcceder']");

		String previousUrl = driver.getCurrentUrl();

		usrBox.sendKeys(usr);
		passBox.sendKeys(passw);
		enterButton.click();

		fluentUrlChanged(previousUrl);

		// Espera por defecto de 4 segundos
		try {
			Thread.sleep(4500);
		} catch (InterruptedException e) {
		}
	}

	public static void clickHiddenButton(WebElement button) {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", button);
	}

}