package app.clinica.managua;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import app.GenericUnit;

public class PaginaAnalisis extends GenericUnit {

	public PaginaAnalisis(WebDriver driver) {
		super(driver);
		Helper.init(driver);
	}

	public void removeAnalisis(String id) {

		// Click en boton eliminar
		Helper.fluentElement("//button[@id='rmAnalisis']").click();

		// Introducimos el id
		Helper.fluentElement("//input[@id='inputDelA']").sendKeys(id);

		// Pulsamos en eliminar
		Helper.fluentElement("//button[@id='ejecutarDelA']").click();

		// Cerramos el menu
		Helper.fluentElement("//button[@id='closeA']").click();

	}

	public List<List<String>> getAnalisis() {
		List<String> analisis = null;
		List<List<String>> res = new ArrayList<>();
		int size = Helper.fluentElements("//table[@id='ASProceso']//td[1]").size();
		for (int i = 0; i < size; i++) { // Una iter por cada entrada
			analisis = new ArrayList<>();
			try {
				analisis.add(Helper.fluentElement("(//table[@id='ASProceso']//td[1])[" + (i + 1) + "]").getText());
			} catch (Exception e) {
				analisis.add(Helper.fluentElement("(//table[@id='ASProceso']//td[1])[" + (i + 1) + "]").getText());
			}
			try {
				analisis.add(Helper.fluentElement("(//table[@id='ASProceso']//td[2])[" + (i + 1) + "]").getText());
			} catch (Exception e) {
				analisis.add(Helper.fluentElement("(//table[@id='ASProceso']//td[2])[" + (i + 1) + "]").getText());
			}
			try {
				analisis.add(Helper.fluentElement("(//table[@id='ASProceso']//td[3])[" + (i + 1) + "]").getText());
			} catch (Exception e) {
				analisis.add(Helper.fluentElement("(//table[@id='ASProceso']//td[3])[" + (i + 1) + "]").getText());
			}
			try {
				analisis.add(Helper.fluentElement("(//table[@id='ASProceso']//td[4])[" + (i + 1) + "]").getText());
			} catch (Exception e) {
				analisis.add(Helper.fluentElement("(//table[@id='ASProceso']//td[4])[" + (i + 1) + "]").getText());
			}
			res.add(analisis);
		}

		return res;
	}

	public boolean isTablaVacia() {
		return Helper.fluentElements("//table[@id='ASProceso']//tr").size() == 1 ? true : false;
	}

	/*
	 * Devolvemos una lista con los ids de las pruebas finalizadas
	 */
	public List<String> getFinalizados() {
		List<String> res = new ArrayList<>();
		Helper.scrollToBottom();
		int nPags = Integer.parseInt(Helper.fluentElement("//h5").getText().split("/")[1]);
		res.addAll(Helper.fluentElements("//table[@id='SangreFinalizado']//td[1]").stream().map(WebElement::getText)
				.collect(Collectors.toList()));
		for (int j = 0; j < nPags - 1; j++) {
			Helper.scrollToBottom();
			Helper.fluentElement("//b[text()='Siguiente -> ']").click();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
			}
			res.addAll(Helper.fluentElements("//table[@id='SangreFinalizado']//td[1]").stream().map(WebElement::getText)
					.collect(Collectors.toList()));
		}
		return res;
	}

	public List<PruebaAS> getPruebasFinalizadasByDNI(String dni) throws InterruptedException {
		List<PruebaAS> res = new ArrayList<>();
		Helper.scrollToBottom();
		Thread.sleep(1000);
		int nPags = Integer.parseInt(Helper.fluentElement("//h5").getText().trim().split("/")[1]);
		List<String> dnis = Helper.fluentElements("//table[@id='SangreFinalizado']//td[3]").stream()
				.map(WebElement::getText).collect(Collectors.toList());
		int[] posBuscadas = IntStream.range(0, dnis.size()).filter(i -> dni.equals(dnis.get(i))).toArray();
		res.addAll(getPruebasByPos(posBuscadas));
		for (int j = 0; j < nPags - 1; j++){
			Helper.scrollToBottom();
			Helper.fluentElement("//b[text()='Siguiente -> ']").click();
			Thread.sleep(1500);
			List<String> dnisAux = Helper.fluentElements("//table[@id='SangreFinalizado']//td[3]").stream()
					.map(WebElement::getText).collect(Collectors.toList());
			posBuscadas = IntStream.range(0, dnisAux.size()).filter(i -> dni.equals(dnisAux.get(i))).toArray();
			res.addAll(getPruebasByPos(posBuscadas));
		}
		return res;
	}

	private List<PruebaAS> getPruebasByPos(int[] posiciones) throws InterruptedException {
		List<PruebaAS> res = new ArrayList<>();
		String id, trabajador, fecha, tipo, glucosa, colesterol, trigliceridos, bilirrubina;
		WebElement boton;
		Actions action = new Actions(driver);
		for (Integer i : posiciones) {
			id = Helper.fluentElement("(//table[@id='SangreFinalizado']//td[1])[" + (i+1) + "]").getText();
			trabajador = Helper.fluentElement("(//table[@id='SangreFinalizado']//td[2])[" + (i+1) + "]").getText();
			fecha = Helper.fluentElement("(//table[@id='SangreFinalizado']//td[4])[" + (i+1) + "]").getText().split(" ")[0];
			tipo = "AnalisisSangre";
			boton = Helper.fluentElement("(//table[@id='SangreFinalizado']//td[5])[" + (i+1) + "]");
			boton.click();
			Thread.sleep(350);
			glucosa = Helper.fluentElement("//div[@id='datos']").getText().split("\n")[1].split(" ")[1];
			colesterol = Helper.fluentElement("//div[@id='datos']").getText().split("\n")[2].split(" ")[1];
			trigliceridos = Helper.fluentElement("//div[@id='datos']").getText().split("\n")[3].split(" ")[1];
			bilirrubina = Helper.fluentElement("//div[@id='datos']").getText().split("\n")[4].split(" ")[1];
			res.add(new PruebaAS(id, trabajador, fecha, tipo, glucosa, colesterol, trigliceridos, bilirrubina));
			action.sendKeys(Keys.ESCAPE).build().perform();
			Thread.sleep(350);
		}
		return res;
	}
}
