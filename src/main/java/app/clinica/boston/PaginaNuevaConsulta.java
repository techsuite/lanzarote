package app.clinica.boston;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import app.GenericUnit;
import app.clinica.dakar.Helper;

public class PaginaNuevaConsulta extends GenericUnit {

	public PaginaNuevaConsulta(WebDriver driver) {
		super(driver);
		Helper.init(driver);
	}

	public void addConsulta(String dni, String enfermedades, String medicamentos, String pruebas, String observaciones)
			throws InterruptedException {
		getBotonConsultas(dni).click();
		String prev = driver.getCurrentUrl();
		Helper.fluentElement("//button[@id='anadir-consulta']").click();
		Helper.fluentUrlChanged(prev);
		List<WebElement> campos = Helper.fluentElements("//textarea");
		campos.get(0).sendKeys(enfermedades);
		campos.get(1).sendKeys(medicamentos);
		campos.get(2).sendKeys(pruebas);
		campos.get(3).sendKeys(observaciones);
		Helper.fluentElement("//button[text()=' Enviar ']").click();
		Thread.sleep(500);
		prev = driver.getCurrentUrl();
		Helper.fluentElement("//button[text()='Sí, adelante']").click();
		Helper.fluentUrlChanged(prev);
		Helper.fluentElement("//button[text()='OK']").click();
	}

	public List<String> getPacientes() {
		return Helper.fluentElements("//table[@id='pacsAsociados']//td[1]").stream().map(WebElement::getText)
				.collect(Collectors.toList());
	}

	public List<Consulta> getConsultas(String dni) {
		List<Consulta> res = new ArrayList<>();
		List<String> ids, enfermedades, medicamentos, pruebas, observaciones, dnisDoctores, fechas;

		// Comprobamos si hay varias paginas
		int nPags = Integer.parseInt(
				Helper.fluentElement("//div[@class='numPagCon']//h5[contains(text(),'/')]").getText().split("/")[1]);

		for (int j = 0; j < nPags; j++) {
			try {
				ids = Helper.fluentElements("//table[@id='consPrevias' and @style='display: block;']//td[1]").stream()
						.map(WebElement::getText).collect(Collectors.toList());
			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				ids = Helper.fluentElements("//table[@id='consPrevias' and @style='display: block;']//td[1]").stream()
						.map(WebElement::getText).collect(Collectors.toList());
			}
			try {
				enfermedades = Helper.fluentElements("//table[@id='consPrevias' and @style='display: block;']//td[2]")
						.stream().map(WebElement::getText).collect(Collectors.toList());
			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				enfermedades = Helper.fluentElements("//table[@id='consPrevias' and @style='display: block;']//td[2]")
						.stream().map(WebElement::getText).collect(Collectors.toList());
			}
			try {
				medicamentos = Helper.fluentElements("//table[@id='consPrevias' and @style='display: block;']//td[3]")
						.stream().map(WebElement::getText).collect(Collectors.toList());
			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				medicamentos = Helper.fluentElements("//table[@id='consPrevias' and @style='display: block;']//td[3]")
						.stream().map(WebElement::getText).collect(Collectors.toList());
			}
			try {
				pruebas = Helper.fluentElements("//table[@id='consPrevias' and @style='display: block;']//td[4]")
						.stream().map(WebElement::getText).collect(Collectors.toList());
			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				pruebas = Helper.fluentElements("//table[@id='consPrevias' and @style='display: block;']//td[4]")
						.stream().map(WebElement::getText).collect(Collectors.toList());
			}
			try {
				observaciones = Helper.fluentElements("//table[@id='consPrevias' and @style='display: block;']//td[5]")
						.stream().map(WebElement::getText).collect(Collectors.toList());
			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				observaciones = Helper.fluentElements("//table[@id='consPrevias' and @style='display: block;']//td[5]")
						.stream().map(WebElement::getText).collect(Collectors.toList());
			}
			try {
				dnisDoctores = Helper.fluentElements("//table[@id='consPrevias' and @style='display: block;']//td[6]")
						.stream().map(WebElement::getText).collect(Collectors.toList());
			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				dnisDoctores = Helper.fluentElements("//table[@id='consPrevias' and @style='display: block;']//td[6]")
						.stream().map(WebElement::getText).collect(Collectors.toList());
			}
			try {
				fechas = Helper.fluentElements("//table[@id='consPrevias' and @style='display: block;']//td[7]")
						.stream().map(WebElement::getText).collect(Collectors.toList());
			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
				fechas = Helper.fluentElements("//table[@id='consPrevias' and @style='display: block;']//td[7]")
						.stream().map(WebElement::getText).collect(Collectors.toList());
			}

			for (int i = 0; i < ids.size(); i++) {
				res.add(new Consulta(ids.get(i), enfermedades.get(i), medicamentos.get(i), pruebas.get(i),
						observaciones.get(i), dnisDoctores.get(i), fechas.get(i)));
			}
		}
		return res;
	}

	public WebElement getBotonConsultas(String dni) {
		List<String> pacientes = getPacientes();
		WebElement button = Helper.fluentElements("//table[@id='pacsAsociados']//td[9]").stream()
				.collect(Collectors.toList()).get(pacientes.indexOf(dni));
		// Hacemos scroll para que el boton este visible
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", button);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
		}
		return button;
	}

	public void filtrar(String dni) {
		WebElement filterBox = Helper.fluentElement("//input");
		filterBox.clear();
		filterBox.sendKeys(dni);
		Helper.fluentElement("//button[text()='Buscar']").click();
	}

	/*
	 * Se desconecta de la pagina y devuelve la nueva url tras la desconexion
	 */
	public String disconnect() {
		String prev = driver.getCurrentUrl();
		Helper.fluentElement("//button[@id='volver-consulta']").click();
		Helper.fluentElement("//button[@class='swal2-confirm swal2-styled']").click();
		Helper.fluentUrlChanged(prev);
		return driver.getCurrentUrl();
	}

	public void avanzarPagina() throws InterruptedException {
		Helper.fluentElement("//b[text()='Siguiente']").click();
		Thread.sleep(200);
	}

	public void retrocederPagina() throws InterruptedException {
		Helper.fluentElement("//b[text()='Anterior']").click();
		Thread.sleep(200);
	}
}
