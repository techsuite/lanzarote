package app.clinica.boston;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import app.GenericUnit;

public class PaginaNuevoPaciente extends GenericUnit {

	public PaginaNuevoPaciente(WebDriver driver) {
		super(driver);
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public String getTitle() {
		return Helper.fluentElement("//h2[text()='Pacientes de otros doctores ']").getText();
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getTopButtonNuevoPaciente() {
		return Helper.fluentElement("//a[text()='Nuevo Paciente']");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getTopButtonNuevaConsulta() {
		return Helper.fluentElement("//a[text()='Nueva Consulta']");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getButtonPacienteNuevoIngreso() {
		return Helper.fluentElement("//button[@id='btn-formulario-paciente']");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getButtonLogout() {
		return Helper.fluentElement("//button[@id='volver-consulta']");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getFilterInput() {
		return Helper.fluentElement("//input");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getButtonBuscar() {
		return Helper.fluentElement("//button[text()='Buscar']");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public List<String> getTableHeader(){
		List<String> nombresColumna = new ArrayList<>();
		List<WebElement> fila = Helper.fluentElements("//table/thead/tr/th");
		for(int i = 0; i < 8; i++)
			nombresColumna.add(fila.get(i).getText());
		return nombresColumna;
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public int getTableLength() {
		String palabra = Helper.fluentElement("//table[@id='pacsNoAsociados']/tbody").getAttribute("childElementCount");
		int longitud = palabra.length();
		int num = 0;
		for(int i = longitud-1; i >= 0; i--)
			num += (palabra.charAt(i)-'0')*(Math.pow(10,longitud-i-1));
		return num;
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public List<String> getTableEntry(int nFila) {
		List<String> datos = new ArrayList<>();
		List<WebElement> fila = Helper.fluentElements("//table/tbody/tr["+(nFila+1)+"]/td");
		for(int i = 0; i < 8; i++)
			datos.add(fila.get(i).getText());
		return datos;
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getButtonAsociar(int nFila) {
		return Helper.fluentElement("//table/tbody/tr["+(nFila+1)+"]/td[9]");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public String getPage() {
		return Helper.fluentElement("//h5").getText();
	}
	
	//Esta funcion tiene que devolver excepcion de elemento no encontrado de forma instantanea para asegurar que no se ha encontrado
	public WebElement getUnsafeNextPageButton() {
		return driver.findElement(By.xpath("//b[text()='Siguiente']"));
	}
	
	//Esta funcion tiene que devolver excepcion de elemento no encontrado de forma instantanea para asegurar que no se ha encontrado
	public WebElement getUnsafeLastPageButton() {
		return driver.findElement(By.xpath("//b[text()='Anterior']"));
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getNextPageButton() {
		WebElement elemento = null;
		try {
			elemento = Helper.fluentElement("//b[text()='Siguiente']");
		}catch(TimeoutException e) {}
		return elemento;
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getLastPageButton() {
		WebElement elemento = null;
		try {
			elemento = Helper.fluentElement("//b[text()='Anterior']");
		}catch(TimeoutException e) {}
		return elemento;
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public void acceptAction() {
		
		new WebDriverWait(driver, Duration.ofSeconds(2))
			.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Sí, adelante']")));
		Helper.fluentElement("//button[text()='Sí, adelante']").click();
		
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public void cancelAction() {
		
		new WebDriverWait(driver, Duration.ofSeconds(2))
			.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No'][2]")));
		Helper.fluentElement("//button[text()='No'][2]").click();
		
		new WebDriverWait(driver, Duration.ofSeconds(2))
			.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='OK']")));
		Helper.fluentElement("//button[text()='OK']").click();
	}
	
}
