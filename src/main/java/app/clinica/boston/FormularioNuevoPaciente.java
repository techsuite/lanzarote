package app.clinica.boston;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import app.GenericUnit;

public class FormularioNuevoPaciente extends GenericUnit {

	public FormularioNuevoPaciente(WebDriver driver) {
		super(driver);
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public String getTitle() {
		return Helper.fluentElement("//h1[text()='Registrar paciente']").getText();
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public String getSubtitle() {
		return Helper.fluentElement("//*[text()=' Introduzca los datos del nuevo paciente a registrar en el sistema. ']").getText();
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getHelpButton() {
		return Helper.fluentElement("//button[@id='ayuda-consulta']");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getReturnButton() {
		return Helper.fluentElement("//button[@id='volver-consulta']");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getSendButton() {
		return Helper.fluentElement("//button[text()=' Enviar ']");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getGenderInput() {
		return Helper.fluentElement("//mat-select");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getGenderOption(int id) {
		String valor;
		if(id==0) valor = "masculino";
		else if(id==1) valor = "femenino";
		else valor = "X";
		return Helper.fluentElement("//mat-option[@value='"+valor+"']");
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public void clickElsewhere() {
		Helper.fluentElement("//mat-card").click();
	}
	
	/* Permite salir del menu de seleccion de genero
	 */
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public void exitPopup() {
		Helper.fluentElement("/html/body/div[2]/div[1]").click();
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public WebElement getInput(int id) {
		return Helper.fluentElement("//input[@id='mat-input-"+id+"']");
	}
	
	public String getInputErrorText(int id) {
		return driver.findElement(By.xpath("//mat-error[@id='mat-error-"+id+"']")).getText();
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public String getInputLabelText(int id) {
		id = id*2+1;
		return Helper.fluentElement("//label[@id='mat-form-field-label-"+id+"']/mat-label").getText();
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public String getHelpTitle() {
		return Helper.fluentElement("//h1[@class='mat-dialog-title']").getText();
	}
	
	//Esta funcion requiere de Helper.init() realizado priori a esta
	public String getHelpText() {
		return Helper.fluentElement("//div[@class='mat-dialog-content']").getText();
	}
	
}
