package app.clinica.boston;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import app.GenericUnit;

public class Login extends GenericUnit{

	public Login(WebDriver driver) {
		super(driver);
	}
	
	public String getTitle() {
		return driver.findElement(By.xpath("//h1[contains(text(),'Login')]")).getText();
	}
	public String getTitleHome() {
		return driver.findElement(By.xpath("//h1[contains(text(),'Bienvenido')]")).getText();
	}
	public String getTextHome() {
		return driver.findElement(By.xpath("//p[contains(text(),'espere')]")).getText();
	}
	
	public String getErrorMsg1() {
		return driver.findElement(By.xpath("//b[contains(text(), 'incorrectos')]")).getText();
	}
	
	public String getErrorMsg2() {
		return driver.findElement(By.xpath("//b[contains(text(), 'DNI')]")).getText();
	}
	
	public void clickOn(String option) {
	    String optionXPath = String.format(
	      "//button[text()='%s']",
	      option
	    );
	    driver.findElement(By.xpath(optionXPath)).click();
	}
	
	public void login(String user, String pssw) {
		WebElement usernameElement = driver.findElement(By.xpath("//input[@type='text']"));
		WebElement passwordElement = driver.findElement(By.xpath("//input[@type='password']"));
		
		usernameElement.sendKeys(user);
		passwordElement.sendKeys(pssw);
		driver.findElement(By.xpath("//b[contains(text(),'Entrar')]")).click();
	}
	
	public void buttonHome() {
		driver.findElement(By.xpath("//b[contains(text(),'Continuar')]")).click();
	}

}
