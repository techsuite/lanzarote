package app.clinica.boston;

import org.openqa.selenium.WebElement;

public class Consulta {

	String id, enfermedades, medicamentos, pruebas, observaciones, dniDoctor, fechaConsulta;

	public Consulta(String id, String enfermedades, String medicamentos, String pruebas, String observaciones,
			String dniDoctor, String fechaConsulta) {
		this.id = id;
		this.enfermedades = enfermedades;
		this.medicamentos = medicamentos;
		this.pruebas = pruebas;
		this.observaciones = observaciones;
		this.dniDoctor = dniDoctor;
		this.fechaConsulta = fechaConsulta;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEnfermedades() {
		return enfermedades;
	}

	public void setEnfermedades(String enfermedades) {
		this.enfermedades = enfermedades;
	}

	public String getMedicamentos() {
		return medicamentos;
	}

	public void setMedicamentos(String medicamentos) {
		this.medicamentos = medicamentos;
	}

	public String getPruebas() {
		return pruebas;
	}

	public void setPruebas(String pruebas) {
		this.pruebas = pruebas;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getDniDoctor() {
		return dniDoctor;
	}

	public void setDniDoctor(String dniDoctor) {
		this.dniDoctor = dniDoctor;
	}

	public String getFechaConsulta() {
		return fechaConsulta;
	}

	public void setFechaConsulta(String fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}

	@Override
	public String toString() {
		return "Consulta [id=" + id + ", enfermedades=" + enfermedades + ", medicamentos=" + medicamentos + ", pruebas="
				+ pruebas + ", observaciones=" + observaciones + ", dniDoctor=" + dniDoctor + ", fechaConsulta="
				+ fechaConsulta + "]";
	}
}
