package app;

import org.openqa.selenium.WebDriver;

public class GenericUnit {

  protected final WebDriver driver;

  public GenericUnit(WebDriver driver) {
    this.driver = driver;
  }
}
